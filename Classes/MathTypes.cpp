#include "MathTypes.h"
#include "cocos2d/cocos/math/CCGeometry.h"

USING_NS_CC_MATH;

const IntVec2 IntVec2::ZERO = { 0, 0 };
const IntRect IntRect::ZERO = { 0, 0, 0, 0 };

void IntVec2::asVec2(Vec2& vec) const
{
	vec.x = (float)x;
	vec.y = (float)y;
}

void IntVec2::set(int _x, int _y)
{
	x = _x;
	y = _y;
}

void IntRect::asRect(Rect& rect) const
{
	rect.setRect((float)x, (float)y, (float)w, (float)h);
}

void IntRect::set(int _x, int _y, int _w, int _h)
{
	x = _x;
	y = _y;
	w = _w;
	h = _h;
}

bool IntRect::contains(int testX, int testY) const
{
	return testX >= x && testX < x + w && testY >= y && testY < y + h;
}

bool IntRect::intersects(const IntRect& other) const
{
	return !(x + w - 1 < other.x || y + h - 1 < other.y || x >= other.x + other.w|| y >= other.y + other.h );
}
