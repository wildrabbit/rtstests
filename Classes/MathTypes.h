#pragma once

namespace cocos2d
{
	class Vec2;
	class Rect;
}

struct IntVec2
{
	int x = 0;
	int y = 0;

	static const IntVec2 ZERO;

	void set(int x = 0, int y = 0);
	void asVec2(cocos2d::Vec2& vec2) const;
};

struct IntRect
{
	int x = 0;
	int y = 0;
	int w = 0;
	int h = 0;

	static const IntRect ZERO;

	void set(int x = 0, int y = 0, int w = 0, int h = 0);
	void asRect(cocos2d::Rect& rect) const;
	bool contains(int x, int y) const;
	bool intersects(const IntRect& other) const;
};

enum class Octant: int
{
	N = 0,
	NW,
	W,
	SW,
	S,
	SE,
	E,
	NE
};



Octant determineOctant(const IntVec2& from, const IntVec2& to);

extern const IntVec2 OctantOffsets[8];
const IntVec2& getOfssetForOctant(Octant oct);