#pragma once

#include <vector>
#include <map>
#include "WorldData.h"
#include "TeamTypes.h"

struct CastleData;
struct UnitData;
struct BuildingData;
struct BuilderData;

using VUnits = std::vector <UnitPtr>;
using VBuildings = std::vector<BuildingPtr>;

struct IntVec2;
struct IntRect;

class EntityDatabase;

namespace cocos2d
{
	class Node;
	class Vec2;
	class Size;
};

class World;

class Team
{
public:
	Team() = default;
	Team(World* world);
	~Team();

	TeamID id() const
	{
		return _teamID;
	}
	void setup(TeamID teamID, const TeamLevelData& levelData);

//	bool areaIntersectsEntity(const IntRect& area) const;
	bool isAllyOf(TeamID otherTeamID) const;
	bool isHostileTo(TeamID otherTeamID) const;

private:
	World* _world;
	TeamID _teamID;
	std::map<TeamID, TeamRelationshipType> _teamStances;
};