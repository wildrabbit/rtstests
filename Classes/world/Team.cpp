#include "Team.h"
#include "World.h"
#include "../database/EntityDatabase.h"
#include "cocos2d.h"

USING_NS_CC;

Team::Team(World* world): _world(world) {}
Team::~Team() {}

void Team::setup(TeamID teamID, const TeamLevelData& levelData)
{
	_teamID = teamID;
	_teamStances = levelData.teamStances;
}

bool Team::isAllyOf(TeamID otherTeamID) const
{
	if (otherTeamID == _teamID) return true;
	auto stanceIter = _teamStances.find(otherTeamID);
	if (stanceIter == _teamStances.end())
	{
		return false;
	}
	return stanceIter->second == TeamRelationshipType::Ally;
}

bool Team::isHostileTo(TeamID otherTeamID) const
{
	if (otherTeamID == _teamID) return false;
	auto stanceIter = _teamStances.find(otherTeamID);
	if (stanceIter == _teamStances.end())
	{
		return false;
	}
	return stanceIter->second == TeamRelationshipType::Hostile;
}
//bool Team::areaIntersectsEntity(const IntRect& area) const
//{
//	return _castle->getMapBounds().intersects(area) || _builder->getMapBounds().intersects(area); // || EXISTING BUILDINGS
//}
