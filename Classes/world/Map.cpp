#include "Map.h"
#include "WorldData.h"
#include "cocos2d.h"
#include "../MathTypes.h"

USING_NS_CC;

//------------ MAP---------//


GameMap::GameMap()
{
}

GameMap::~GameMap()
{
}

IntVec2 GameMap::getMapSizeInTiles() const
{
	Size cocosSize =_tileMap->getMapSize();
	return { (int)cocosSize.width, (int)cocosSize.height };
}

IntVec2 GameMap::getTileSize() const
{
	return _tileSize;
}

bool GameMap::isBuildableArea(const IntRect& area) const
{
	Size cocosSize = _tileMap->getMapSize();

	int maxX = area.x + area.w - 1;
	if (maxX >= cocosSize.width) return false;
	
	int maxY = area.y + area.h - 1;
	if (maxY >= cocosSize.height) return false;

	for (int y = area.y; y <= maxY; ++y)
	{
		for (int x = area.x; x <= maxX; ++x)
		{
			if (!isWalkableTile({ x,y }))
				return false;
		}
	}
	return true;
}

bool GameMap::isWalkableTile(const IntVec2& coordsVec) const
{
	// First, rule out impassable ids
	int idOffset = _envLayer->getTileSet()->_firstGid;
	Vec2 coords;
	coordsVec.asVec2(coords);
	int tileID = _envLayer->getTileGIDAt(coords);
	if (tileID > 0)
	{
		tileID -= idOffset;
	}
	if (std::any_of(_impassableTileIDs.begin(), _impassableTileIDs.end(), [tileID](int value) { return tileID == value; }))
	{
		return false;
	}

	return true;
}

void GameMap::buildWalkabilityMap()
{
	using TTileCoords = std::tuple<int, int>;
	IntVec2 mapSize = getMapSizeInTiles();

	size_t numIDs = _impassableTileIDs.size();
	int mapWidth = mapSize.x;
	int mapHeight = mapSize.y;

	const int kMaxCost = std::numeric_limits<int>::max();
	const int kRoadcost = 2;
	const int kGrassCost = 5;
	std::vector<int> walkability(mapWidth * mapHeight, kGrassCost);

	for (int rowIdx = 0; rowIdx < mapHeight; ++rowIdx)
	{
		int baseRowIdx = mapWidth * rowIdx;
		for (int colIdx = 0; colIdx < mapWidth; ++colIdx)
		{
			// Update costs with bg layer (dirt cheaper than grass)
			int cell = _envLayer->getTileGIDAt({ (float)colIdx, (float)rowIdx });
			int idx = baseRowIdx + colIdx;

			if (cell == _roadTileID)
			{
				walkability[idx] = kRoadcost;
			}
			else
			{
				bool exists = false;
				for (size_t k = 0; k < numIDs; ++k)
				{
					if (cell == _impassableTileIDs[k])
					{
						walkability[idx] = kMaxCost;
					}
				}
			}
		}
	}
}


bool GameMap::setup(const MapLevelData& data, Node* root)
{
	_tileMap = TMXTiledMap::create("testCF.tmx");
	if (_tileMap == nullptr)
	{
		return false;
	}
	_tileSize = data.tileSize;
	_impassableTileIDs = data.impassableIDs;
	_roadTileID = data.roadID;
	_bgLayer = _tileMap->getLayer(data.bgLayerName);
	_envLayer = _tileMap->getLayer(data.environmentLayerName);
	root->addChild(_tileMap, 0);

	buildWalkabilityMap();	
	return true;
}
