#pragma once
#include "TeamTypes.h"
#include "../MathTypes.h"
#include "entities/EntityTypes.h"
#include <vector>
#include <map>

struct EntityLevelData
{
	EntityID id;
	IntVec2 startPos;
};

enum class TeamRelationshipType: int
{
	Ally,
	Hostile,
	Neutral
};

struct TeamLevelData
{
	TeamID id;
	bool inputControlled; // false => AI
	std::vector<EntityLevelData> entities;
	std::map<TeamID, TeamRelationshipType> teamStances;
};

struct MapLevelData
{
	std::string mapPath;
	IntVec2 tileSize;
	std::vector<int> impassableIDs;
	int roadID;
	std::string bgLayerName;
	std::string environmentLayerName;
};

using VTeamLevelData = std::vector<TeamLevelData>;

struct WorldLevelData
{
	MapLevelData mapData;
	VTeamLevelData teams;
};

