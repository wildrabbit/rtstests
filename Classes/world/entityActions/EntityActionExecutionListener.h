#pragma once

class Entity;

class CustomEntityActionExecutionData
{
	virtual ~CustomEntityActionExecutionData() = default;
};

class IEntityActionExecutionListener
{
public:
	virtual void onExecutionBegan(Entity* e) = 0;
	virtual void onExecutionFinished(Entity* e) = 0;
	virtual void onExecutionInterrupted(Entity* e) = 0;

	virtual void onCustom(Entity* e, CustomEntityActionExecutionData* data) = 0;
};
