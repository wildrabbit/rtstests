#pragma once
#include "../entities/EntityTypes.h"
#include <string>
#include <vector>


struct EntityAction;
using EntityActionPtr = std::unique_ptr<EntityAction>;

struct EntityAction
{
	EntityActionType type = EntityActionType::Undefined;
	std::string iconPath;
	int cost = 0; // For now, keep as int
	std::string name;

	EntityAction(EntityActionType _type, const std::string& _iconPath, int _cost, const std::string& _name);
	EntityAction() = default;
	virtual ~EntityAction() = default;
	virtual EntityActionPtr clone();
};

using VEntityActions = std::vector<EntityActionPtr>;

struct ConstructBuildingAction : EntityAction
{
	EntityID unitID = -1;
	ConstructBuildingAction() = default;
	ConstructBuildingAction(const std::string& iconPath, int cost, const std::string& name, EntityID unitID);
	EntityActionPtr clone() override;
};

struct SpawnUnitAction : EntityAction
{
	EntityID unitID = -1;
	SpawnUnitAction() = default;
	SpawnUnitAction(const std::string& iconPath, int cost, const std::string& name, EntityID unitID);
	EntityActionPtr clone() override;
};

