#include "EntityActions.h"

SpawnUnitAction::SpawnUnitAction(const std::string& _iconPath, int _cost, const std::string& _name, EntityID _unitID)
	: EntityAction(EntityActionType::SpawnEntity, _iconPath, _cost, _name)
	, unitID(_unitID)
{
}

ConstructBuildingAction::ConstructBuildingAction(const std::string& iconPath, int cost, const std::string& name, EntityID unitID)
	: EntityAction(EntityActionType::ConstructBuilding, iconPath, cost, name)
	, unitID(unitID)
{
}

EntityActionPtr ConstructBuildingAction::clone()
{
	EntityActionPtr leClone = std::make_unique<ConstructBuildingAction>(this->iconPath, this->cost, this->name, this->unitID);
	return leClone;
}

EntityAction::EntityAction(EntityActionType _type, const std::string& _iconPath, int _cost, const std::string& _name)
	: type(_type), iconPath(_iconPath), cost(_cost), name(_name)
{
}

EntityActionPtr EntityAction::clone()
{
	EntityActionPtr leClone = std::make_unique<EntityAction>(this->type, this->iconPath, this->cost, this->name);
	return leClone;
}

EntityActionPtr SpawnUnitAction::clone()
{
	EntityActionPtr leClone = std::make_unique<SpawnUnitAction>(this->iconPath, this->cost, this->name, this->unitID);
	return leClone;
}