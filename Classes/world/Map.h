#pragma once
#include <vector>
#include "MathTypes.h"

using CostVector = std::vector<int>;

namespace cocos2d
{
	class Node;
	class TMXTiledMap;
	class TMXLayer;
	class Size;
};

struct MapLevelData;

class GameMap
{
public:
	GameMap();
	~GameMap();
public:
	IntVec2 getMapSizeInTiles() const;
	IntVec2 getTileSize() const;
	bool setup(const MapLevelData& data, cocos2d::Node* root);
	bool isWalkableTile(const IntVec2& coordsVec) const;
	bool isBuildableArea(const IntRect& area) const;
private:
	void buildWalkabilityMap();

private:
	cocos2d::TMXTiledMap* _tileMap = nullptr;
	cocos2d::TMXLayer* _bgLayer = nullptr;
	cocos2d::TMXLayer* _envLayer = nullptr;
	CostVector _walkability;

	std::vector<int> _impassableTileIDs;
	int _roadTileID;
	IntVec2 _tileSize;
};
