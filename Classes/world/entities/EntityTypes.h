#pragma once
#include <memory>

enum class EntityType: int
{
	None = -1,
	Castle,
	Builder,
	Building,
	Warrior
};

typedef int EntityID;

class Entity;
using EntityPtr = std::shared_ptr<Entity>;

class Building;
using BuildingPtr = std::shared_ptr<Building>;

class Unit;
using UnitPtr = std::shared_ptr<Unit>;

class Builder;
using BuilderPtr = std::shared_ptr<Builder>;

class Castle;
using CastlePtr = std::shared_ptr<Castle>;


enum class EntityActionType : int
{
	Undefined = -1,
	MoveTo,
	MoveAttackTarget,
	ConstructBuilding,
	RepairBuilding,
	BuildUnit,
	HealUnit,
	SpawnEntity,
	HealRepairEntity,
	AbortCurrent
	//UpgradeUnit

};

enum class EntityActionExecutionStatus: int
{
	Began = 0,
	Finished,
	Interrupt,
	Custom
};