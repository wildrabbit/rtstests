#pragma once

#include "Entity.h"
#include "../entityActions/EntityActions.h"
#include <vector>

enum class BuildingState : int
{
	Construction = 0,
	Idle,
	Damaged,
	Destroyed
	// TODO: BUSY
};

struct BuildingData: EntityData
{
	BuildingData() = default;
	BuildingData(const EntityData& baseData);
	virtual ~BuildingData() = default;
};

constexpr int kNumStates = (int)BuildingState::Destroyed - (int)BuildingState::Construction + 1;

class Building: public Entity
{
private:
	const BuildingData* _buildingData = nullptr;
	int _defaultBuildingAction = -1;
	EntityID _buildingTargetID = -1;
	BuildingState currentState = BuildingState::Construction;
	cocos2d::SpriteFrame* frames[kNumStates];

	// spawning time!
	float _spawnElapsed = 0.f;
	float _spawnDelay = 0.f;
	
	bool _spawnPaused = false; // Only for construction stage;
	bool _requiresBuilder = true;
	int _numBuilders = 0;

public:
	Building(World* world);
	~Building();
	
	void onSetup(const EntityLevelData& levelData) override;
	void createView(cocos2d::Node* node) override;
	void update(float dt) override;
	
};