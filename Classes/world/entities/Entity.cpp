#include "Entity.h"
#include "../Team.h"
#include "../World.h"
#include "../WorldData.h"
#include "../WorldUtils.h"
#include "../Map.h"
#include "../../inputContexts/InputContexts.h"
#include "../../GameOptions.h"
#include "cocos2d.h"
#include <iostream>

USING_NS_CC;

EntityData::EntityData(EntityID _ID, EntityType _type, const std::string& _name, const IntVec2& _size, const std::string& _path, float _maxHP, float _spawnTime, const VEntityActions& entityActionData, int defaultActionIdx)
	: ID(_ID), typeID(_type), name(_name), size(_size), path(_path), maxHP(_maxHP), spawnTime(_spawnTime), defaultEntityActionIdx(defaultActionIdx)
{
	entityActions.reserve(entityActionData.size());
	for (size_t i = 0; i < entityActionData.size(); ++i)
	{
		entityActions.push_back(entityActionData[i]->clone());
	}
}

EntityData::EntityData(const EntityData& other)
{
	this->ID = other.ID;
	this->spawnTime = other.spawnTime;
	this->maxHP = other.maxHP;
	this->path = other.path;
	this->size = other.size;
	this->typeID = other.typeID;
	this->name = other.name;
	this->defaultEntityActionIdx = other.defaultEntityActionIdx;

	entityActions.reserve(other.entityActions.size());
	for (size_t i = 0; i < other.entityActions.size(); ++i)
	{
		entityActions.push_back(std::move(other.entityActions[i]->clone()));
	}
}

EntityData::EntityData(EntityData&& other)
{
	this->ID = other.ID;
	this->spawnTime = other.spawnTime;
	this->maxHP = other.maxHP;
	this->path = other.path;
	this->size = other.size;
	this->typeID = other.typeID;
	this->name = other.name;
	this->defaultEntityActionIdx = other.defaultEntityActionIdx;

	entityActions.reserve(other.entityActions.size());
	for (size_t i = 0; i < other.entityActions.size(); ++i)
	{
		entityActions.push_back(std::move(other.entityActions[i]));
	}
}

EntityData& EntityData::operator=(const EntityData& other)
{
	this->ID = other.ID;
	this->spawnTime = other.spawnTime;
	this->maxHP = other.maxHP;
	this->path = other.path;
	this->size = other.size;
	this->typeID = other.typeID;
	this->name = other.name;
	this->defaultEntityActionIdx = other.defaultEntityActionIdx;

	entityActions.reserve(other.entityActions.size());
	for (size_t i = 0; i < other.entityActions.size(); ++i)
	{
		entityActions.push_back(std::move(other.entityActions[i]->clone()));
	}
	return *this;
}

EntityData& EntityData::operator=(EntityData&& other)
{
	this->ID = other.ID;
	this->spawnTime = other.spawnTime;
	this->maxHP = other.maxHP;
	this->path = other.path;
	this->size = other.size;
	this->typeID = other.typeID;
	this->name = other.name;
	this->defaultEntityActionIdx = other.defaultEntityActionIdx;

	entityActions.reserve(other.entityActions.size());
	for (size_t i = 0; i < other.entityActions.size(); ++i)
	{
		entityActions.push_back(std::move(other.entityActions[i]));
	}
	return *this;
}


//--------------------------------------

Entity::Entity(World* world)
: _world(world)
{
}

Entity::~Entity()
{
	view->removeFromParent();
}

int Entity::sNextId = 1;
int Entity::getNextId()
{ 
	return ++sNextId; 
}

void Entity::setup(const EntityData* entityData, const EntityLevelData& levelData, int teamID, cocos2d::Node* parent)
{
	data = entityData;
	_instanceID = getNextId();
	_teamID = teamID;
	hp = entityData->maxHP;
	_actionIdx = entityData->defaultEntityActionIdx;

	onSetup(levelData);
	createView(parent);
	setCoords(levelData.startPos);
	initActionMappings();
}

void Entity::setCoords(int newX, int newY)
{
	tileCoords.set(newX, newY);
	pos = worldutils::coordsToWorldPos({ newX, newY }, _world);
	if (view)
	{
		view->setPosition(pos);
	}
}

void Entity::setCoords(const IntVec2& tile)
{
	setCoords(tile.x, tile.y);
}


IntRect Entity::getMapBounds() const
{
	return { tileCoords.x, tileCoords.y, data->size.x, data->size.y };
}

bool Entity::intersects(const cocos2d::Vec2& location) const
{
	return view->getBoundingBox().containsPoint(location);
}

void Entity::update(float dt)
{
}

void Entity::registerToWorld(World* world)
{
	world->addEntity(shared_from_this());
}

void Entity::unregisterFromWorld(World* world)
{
	world->removeEntity(shared_from_this());
}

void Entity::onSelected(bool interactable)
{
	std::cout << "Selected " << _instanceID << " (" << data->name << ")" << std::endl;
}

void Entity::onDeselected()
{
	std::cout << "Deselected " << _instanceID << " (" << data->name << ")" << std::endl;
}

EntityActionType Entity::getActionType() const
{
	if (_actionIdx >= 0 &&  (size_t)_actionIdx < data->entityActions.size())
		return data->entityActions[_actionIdx]->type;
	else
	{
		return EntityActionType::Undefined;
	}
}

EntityActionType Entity::getDefaultActionType() const
{
	if (data->defaultEntityActionIdx >= 0 && (size_t)data->defaultEntityActionIdx < data->entityActions.size())
		return data->entityActions[data->defaultEntityActionIdx]->type;
	else
	{
		return EntityActionType::Undefined;
	}
}

bool Entity::isActionTypeSupported(EntityActionType type) const
{
	return std::find_if(data->entityActions.begin(), data->entityActions.end(), [type](const EntityActionPtr& action)
	{
		return action->type == type;
	}) != data->entityActions.end();
}

EntityID Entity::getId() const
{
	return _instanceID;
}

bool Entity::isHostileTo(const Entity* other) const
{
	return _world->getTeamById(_teamID)->isHostileTo(other->_teamID);
}

bool Entity::isAllyOf(const Entity* other) const
{
	return _world->getTeamById(_teamID)->isAllyOf(other->_teamID);
}


void Entity::initActionMappings()
{
}

const std::map<InputActionKey, EntityActionType>& Entity::getActionMappings() const
{
	return _actionMappings;
}

void Entity::executeAction(const InputContext* inputContext)
{
	_world->onEntityBeganAction(this);
}

void Entity::stopAction()
{
}

bool Entity::canExecute(const InputContext* inputContext) const
{
	return false;
}