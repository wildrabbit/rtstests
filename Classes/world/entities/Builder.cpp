#include "Builder.h"
#include "../World.h"
#include "../Map.h"
#include "../WorldData.h"
#include "../WorldUtils.h"
#include "../../GameOptions.h"
#include "cocos2d.h"
#include "../../inputContexts/InputContexts.h"

USING_NS_CC;

const float SQRT_2 = 1.41421356237f;

BuilderData::BuilderData(const EntityData& baseData, float _speed, const std::vector<EntityID>& buildingIDs, int defaultIdx)
	:EntityData(baseData)
	,speed(_speed)
	,buildingIDs(buildingIDs)
	,currentBuildingIdx(defaultIdx)
{}

Builder::Builder(World* world)
	:Entity(world)
{
}

void Builder::onSetup(const EntityLevelData& levelData)
{
	builderData = dynamic_cast<const BuilderData*>(data);
	_buildingID = builderData->buildingIDs[builderData->currentBuildingIdx];
}

void Builder::createView(cocos2d::Node* parent)
{
	const IntVec2 tileSize = _world->map()->getTileSize();
	view = Sprite::createWithSpriteFrame(SpriteFrame::create(data->path, { 0.f, 0.f, (float)(tileSize.x * data->size.x), (float)(tileSize.y * data->size.y) }));
	view->setAnchorPoint({ 0.f, 1.f });

	if (parent)
	{
		parent->addChild(view, 6);
	}
}

void Builder::setClosestTarget(const IntRect& area, IntVec2* targetCoords)
{
	int maxX = area.x + area.w;
	int maxY = area.y + area.h;
	
	int tgtX = area.x;
	if (tileCoords.x >= maxX)
	{
		tgtX = maxX;
	}
	else if (tileCoords.x < area.x)
	{
		tgtX = area.x - 1;
	}
	else
	{
		tgtX = tileCoords.x;
	}

	int tgtY = area.y;
	if (tileCoords.y >= maxY)
	{
		tgtY = maxY;
	}
	else if (tileCoords.y < area.y)
	{
		tgtY = area.y - 1;
	}
	else
	{
		tgtY = tileCoords.y;
	}
	if (targetCoords != nullptr)
	{
		targetCoords->set(tgtX, tgtY);
	}
	setTarget(tgtX, tgtY);
}

void Builder::setTarget(int x, int y)
{
	const IntVec2 tileSize = _world->map()->getTileSize();
	const IntVec2 winSize = _world->options()->winSize;
	//if (context == BuilderActionContext::Move)
	{
		tgtCoords.set(x, y);
		nextPos.set((float)x * tileSize.x, (float)(winSize.y - y * tileSize.y));
	}


	velocity = (nextPos - pos).getNormalized();
	// When we switch to pf, check if we need to add the else() here
	state = BuilderState::Moving;
}

void Builder::setTarget(const IntVec2& newTarget)
{
	setTarget(newTarget.x, newTarget.y);
}

void Builder::setSelected(bool selected)
{
	Color3B colour = selected ? Color3B::YELLOW : Color3B::WHITE;
	view->setColor(colour);
}

void Builder::update(float dt)
{
	CCASSERT(builderData != nullptr, "Invalid data!");
	if (state == BuilderState::Moving)
	{
		if (nextPos.distance(pos) <= 5.f)
		{
			velocity = Vec2::ZERO;
			pos = nextPos;

			if (_targetCallback != nullptr)
			{
				_targetCallback(this);
			}

			switch (_currentActionType)
			{
				case EntityActionType::MoveTo:
				{
					state = BuilderState::Idle;
					_currentActionType = EntityActionType::Undefined;
					_world->onEntityFinishedAction(this);
					break;
				}
				case EntityActionType::ConstructBuilding:
				{
					state = BuilderState::Building; // Construction!!
					_world->removeBlueprint(_blueprint);
					_blueprint = nullptr;
					_world->spawnEntity(_buildingID, actionTargetCoords, _teamID);
				}
			}
		}
		else
		{
			pos += velocity * builderData->speed * dt;
		}
		tileCoords = worldutils::worldPosToCoords(pos, _world);
		view->setPosition(pos);
	}
	else if (state == BuilderState::Building)
	{
		// if (Timeout -> Finalise action)
		state = BuilderState::Idle;
		actionTargetCoords.set(-1, -1);
		_currentActionType = EntityActionType::Undefined;
		_world->onEntityFinishedAction(this);
	}
}

void Builder::setOnReachedTarget(const std::function<void(Builder* builder)>& targetCallback)
{
	_targetCallback = targetCallback;
}

void Builder::onSelected(bool interactable)
{
	setSelected(interactable);
}

void Builder::onDeselected()
{
	setSelected(false);
}

void Builder::executeAction(const InputContext* inputContext)
{
	const EntitySelectionContext* entitySelection = dynamic_cast<const EntitySelectionContext*>(inputContext);
	if (!entitySelection)
	{
		_currentActionType = EntityActionType::Undefined;
		return;
	}

	_currentActionType = entitySelection->actionID;
	if (entitySelection->actionID == EntityActionType::MoveTo)
	{
		setTarget(entitySelection->actionParamCoords);		
	}
	else if (entitySelection->actionID == EntityActionType::ConstructBuilding)
	{
		actionTargetCoords = entitySelection->actionParamCoords;
		IntRect buildingArea = _world->getBuildingAreaAtCoords(actionTargetCoords, _buildingID);
		setClosestTarget(buildingArea);
		_blueprint = _world->placeBlueprint(_buildingID, actionTargetCoords, _teamID);	
	}
	else if (entitySelection->actionID == EntityActionType::RepairBuilding)
	{
		CCLOG("WILL REPAIR Entity %d - (%s)\n", entitySelection->actionParamEntity->getId(), entitySelection->actionParamEntity->data->name.c_str());
	}

	Entity::executeAction(inputContext);
}

void Builder::initActionMappings()
{
	_actionMappings = {
		{ InputActionKey::Move, EntityActionType::MoveTo},
		{ InputActionKey::Build, EntityActionType::ConstructBuilding},
		{ InputActionKey::Repair, EntityActionType::RepairBuilding},
		{ InputActionKey::Cancel, EntityActionType::AbortCurrent}
	};
}

void Builder::stopAction()
{
	// Check action
}

bool Builder::canExecute(const InputContext* inputContext) const
{
	const EntitySelectionContext* entitySelection = dynamic_cast<const EntitySelectionContext*>(inputContext);
	if (!entitySelection)
	{
		return false;
	}
	
	switch (entitySelection->actionID)
	{
		case EntityActionType::MoveAttackTarget:
		case EntityActionType::MoveTo:			
		case EntityActionType::ConstructBuilding:
		{
			const bool validCoords = entitySelection->actionParamCoords.x >= 0 && entitySelection->actionParamCoords.y >= 0;
			return validCoords && _world->isWalkableTile(entitySelection->actionParamCoords) && !_world->isOcuppiedTile(entitySelection->actionParamCoords);			
		}
		case EntityActionType::HealRepairEntity:
		{
			return entitySelection->actionParamEntity != nullptr;
		}
		case EntityActionType::AbortCurrent:
		{
			return entitySelection->actionID != EntityActionType::Undefined;
		}
		default:
			return false;
	}
}

EntityID Builder::getTargetBuildingID() const
{
	return _buildingID;
}