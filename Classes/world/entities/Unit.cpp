#include "Unit.h"
#include "../World.h"
#include "../WorldData.h"
#include "../Map.h"
#include "cocos2d.h"

USING_NS_CC;

const float SQRT_2 = 1.41421356237f;

UnitData::UnitData(const EntityData& baseData, float _moveSpeed, float _spawnTime, float _baseAttack, float _baseDefense, float _attackRate)
	:EntityData(baseData)
	, moveSpeed(_moveSpeed)
	, spawnTime(_spawnTime)
	, baseAttack(_baseAttack)
	, baseDefense(_baseDefense)
	, attackRate(_attackRate)
{}

Unit::Unit(World* world)
	:Entity(world)
{
}

void Unit::onSetup(const EntityLevelData& levelData)
{
	unitData = dynamic_cast<const UnitData*>(data);
}

void Unit::createView(cocos2d::Node* parent)
{
	const IntVec2 tileSize = _world->map()->getTileSize();
	view = Sprite::createWithSpriteFrame(SpriteFrame::create(data->path, { 0.f, 0.f, (float)(tileSize.x * data->size.x), (float)(tileSize.y * data->size.y) }));
	view->setAnchorPoint({ 0.f, 1.f });

	if (parent)
	{
		parent->addChild(view, 6);
	}
}

void Unit::update(float dt)
{
}

void Unit::registerToWorld(World* world)
{
	Entity::registerToWorld(world);
	world->addUnit(dynamic_pointer_cast<Unit>(shared_from_this()));
}

void Unit::unregisterFromWorld(World* world)
{
	Entity::unregisterFromWorld(world);
	world->removeUnit(dynamic_pointer_cast<Unit>(shared_from_this()));
}

//void Unit::setClosestTarget(const IntRect& area)
//{
//	int maxX = area.x + area.w;
//	int maxY = area.y + area.h;
//
//	int tgtX = area.x;
//	if (tileCoords.x >= maxX)
//	{
//		tgtX = maxX;
//	}
//	else if (tileCoords.x < area.x)
//	{
//		tgtX = area.x - 1;
//	}
//	else
//	{
//		tgtX = tileCoords.x;
//	}
//
//	int tgtY = area.y;
//	if (tileCoords.y >= maxY)
//	{
//		tgtY = maxY;
//	}
//	else if (tileCoords.y < area.y)
//	{
//		tgtY = area.y - 1;
//	}
//	else
//	{
//		tgtY = tileCoords.y;
//	}
//	setTarget(tgtX, tgtY);
//}
//
//void Builder::setTarget(int x, int y)
//{
//	//if (context == BuilderActionContext::Move)
//	{
//		actionTargetCoords.set();
//		tgtCoords.set(x, y);
//		nextPos.set((float)x * data->tileSize.x, (float)(winSize.y - y * data->tileSize.y));
//	}
//
//
//	velocity = (nextPos - pos).getNormalized();
//	// When we switch to pf, check if we need to add the else() here
//	state = BuilderState::Moving;
//}
//
//void Builder::setTarget(const IntVec2& newTarget)
//{
//	setTarget(newTarget.x, newTarget.y);
//}
//
//void Builder::setSelected(bool selected)
//{
//	Color3B colour = selected ? Color3B::YELLOW : Color3B::WHITE;
//	view->setColor(colour);
//}
//
//void Builder::update(float dt)
//{
//	CCASSERT(builderData != nullptr, "Invalid data!");
//	if (state == BuilderState::Moving)
//	{
//		if (nextPos.distance(pos) <= 5.f)
//		{
//			velocity = Vec2::ZERO;
//			pos = nextPos;
//
//			if (_targetCallback != nullptr)
//			{
//				_targetCallback(this);
//			}
//		}
//		else
//		{
//			pos += velocity * builderData->speed * dt;
//		}
//
//		view->setPosition(pos);
//	}
//}
//
//
//
//void Builder::setContext(BuilderActionContext newContext)
//{
//	context = newContext;
//}
//
//BuilderActionContext Builder::getContext() const
//{
//	return context;
//}
//
//void Builder::setOnReachedTarget(const std::function<void(Builder* builder)>& targetCallback)
//{
//	_targetCallback = targetCallback;
//}