#pragma once

#include "Entity.h"

#include "cocos2d/cocos/math/Vec2.h"

namespace cocos2d
{
	class Node;
};

struct CastleData: EntityData
{	
	CastleData() = default;
	CastleData(const EntityData& baseData);
	virtual ~CastleData() = default;
};

extern const int kDefaultState;
extern const int kDamagedState;
extern const int kDestroyedState;

class Castle: public Entity
{
public:
	Castle(World*);
	~Castle();
	int currentState;
	cocos2d::SpriteFrame* frames[3];
	void onSetup(const EntityLevelData& levelData) override;
	void createView(cocos2d::Node* node) override;
	void setSelectionState(bool state);
	void update(float dt) override;

	void onSelected(bool interactable) override;
	void onDeselected() override;
};
