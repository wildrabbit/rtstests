#include "Building.h"
#include "../World.h"
#include "../WorldData.h"
#include "../Map.h"
#include "cocos2d.h"

USING_NS_CC;


BuildingData::BuildingData(const EntityData& entityData)
	:EntityData(entityData)
{}


Building::Building(World* world)
: Entity(world)
{
}

Building::~Building()
{
	for (SpriteFrame* frame: frames)
	{
		frame->release();
	}
}

void Building::onSetup(const EntityLevelData& levelData)
{
	_buildingData = dynamic_cast<const BuildingData*>(data);	
	
	// Setup default action
	if (_buildingData->entityActions.at(_actionIdx)->type == EntityActionType::SpawnEntity)
	{
		const SpawnUnitAction* spawnActionData = dynamic_cast<SpawnUnitAction*>(_buildingData->entityActions.at(_actionIdx).get());
		if (spawnActionData)
		{
			EntityID unitID = spawnActionData->unitID;

		}
	}

	const IntVec2 tileSize = _world->map()->getTileSize();
	float width = data->size.x * tileSize.x;
	float height = data->size.y * tileSize.y;
	for (int i = 0; i < kNumStates; ++i)
	{
		frames[i] = SpriteFrame::create(data->path, { i * width, 0.f, width, height });
		frames[i]->retain();
	}

	_numBuilders = 1;
	_spawnDelay = _buildingData->spawnTime;
	_spawnPaused = _requiresBuilder && _numBuilders == 0;
	if (!_spawnPaused)
	{
		_spawnElapsed = 0.f;
	}
	currentState = BuildingState::Construction;
}

void Building::createView(cocos2d::Node* parent)
{
	view = Sprite::createWithSpriteFrame(frames[(int)currentState]);
	view->setAnchorPoint({ 0.f, 1.f });

	if (parent) parent->addChild(view, 5);
}

void Building::update(float dt)
{
	Entity::update(dt);
	switch (currentState)
	{
		case BuildingState::Construction:
		{
			bool isPaused = _requiresBuilder && _numBuilders == 0;
			if (!isPaused && _spawnPaused && _spawnElapsed < 0)
			{
				_spawnElapsed = 0.f;
			}
			_spawnPaused = isPaused;
			if (!_spawnPaused)
			{
				_spawnElapsed += dt;
				if (_spawnElapsed >= _spawnDelay)
				{
					_spawnElapsed = -1.f;
					currentState = BuildingState::Idle;
					view->setSpriteFrame(frames[(int)currentState]);
				}
			}
			break;
		}
	}
	//BuildingAction* action = _buildingData->buildingActionData.at(_defaultBuildingAction).get();
	//if (!action)
	//{		
	//	return;
	//}

	//switch (action->type)
	//{
	//	case BuildingActionType::SpawnUnit:
	//	{
	//		break;
	//	}
	//	default:break;
	//}
}