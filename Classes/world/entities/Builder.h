#pragma once

#include "Entity.h"
#include <vector>

enum class BuilderState: int
{
	Idle = 0,
	Moving,
	Building
};

enum class BuilderActionContext : int
{
	Move = 0,
	Build,
	Repair
};

struct BuilderData: EntityData
{
	float speed = 100.f;
	std::vector<EntityID> buildingIDs;
	int currentBuildingIdx;

	BuilderData() = default;
	BuilderData(const EntityData& baseData, float _speed, const std::vector<EntityID>& buildingIDs, int defaultIdx);

	virtual ~BuilderData() = default;
};

class Builder: public Entity
{
	using VCoords = std::vector<IntVec2>;
public:
	Builder(World* _world);
	~Builder() = default;

private:
	BuilderState state = BuilderState::Idle;
	
	// Motion
	IntVec2 nextCoords;
	cocos2d::Vec2 nextPos;
	IntVec2 tgtCoords;

	// Action placement
	IntVec2 actionTargetCoords;
	
	cocos2d::Vec2 velocity;
	cocos2d::Vec2 acceleration;

	std::function<void(Builder*)> _targetCallback = nullptr;
	const BuilderData* builderData = nullptr;

	EntityID _buildingID = -1;

	cocos2d::Sprite* _blueprint = nullptr;

protected:
	void initActionMappings() override;

public:
	EntityID getTargetBuildingID() const;
	void onSetup(const EntityLevelData& levelData) override;
	void createView(cocos2d::Node* parent) override;
	
	void setOnReachedTarget(const std::function<void(Builder* builder)>& targetCallback);
	void setClosestTarget(const IntRect& area, IntVec2* targetCoords = nullptr);
	void setTarget(int x, int y);
	void setTarget(const IntVec2& tgt);
	void setSelected(bool selected);
	void update(float dt) override;
	void onSelected(bool interactable) override;
	void onDeselected() override;

	void executeAction(const InputContext* inputContext) override;
	void stopAction() override;
	bool canExecute(const InputContext* inputContext) const override;
};