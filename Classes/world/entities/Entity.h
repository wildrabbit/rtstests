#pragma once

#include <string>
#include <map>
#include "MathTypes.h"
#include "EntityTypes.h"
#include "../entityActions/EntityActions.h"
#include "../../inputContexts/InputContextDefs.h"

namespace cocos2d
{
	class Sprite;
	class SpriteFrame;
	class Node;
	class Size;
};

#include "cocos2d/cocos/math/Vec2.h"

struct EntityLevelData;
struct EntityData
{
	EntityID ID = 0;
	EntityType typeID = EntityType::None;
	std::string name;

	IntVec2 size = { 1, 1 };

	std::string path = "";
	float maxHP = 0.f;

	VEntityActions entityActions;
	int defaultEntityActionIdx = -1;

	float spawnTime = 0.f;

	EntityData() = default;
	EntityData(EntityID _ID, EntityType _type, const std::string& name, const IntVec2& _size, const std::string& _path, float _maxHP, float _spawnTime, const VEntityActions& entityActions, int defaultActionIdx);
	virtual ~EntityData() = default;
	
	EntityData& operator=(const EntityData& other);
	EntityData& operator=(EntityData&& other);
	EntityData(const EntityData& other);
	EntityData(EntityData&& other);
};

struct InputContext;
class World;

class Entity: public std::enable_shared_from_this<Entity>
{
private:
	static int sNextId;

public:
	static int getNextId();

	Entity(World* world);
	virtual ~Entity();

	VEntityActions buildingActionData;
	int startDefaultBuildingAction = -1;


	const EntityData* data;
	float hp;
	IntVec2 tileCoords;
	cocos2d::Vec2 pos;
	
	cocos2d::Sprite* view;

	void setup(const EntityData* entityData, const EntityLevelData& levelData, int teamID, cocos2d::Node* parent);
	void setCoords(int tileX, int tileY);
	void setCoords(const IntVec2& tile);

	IntRect getMapBounds() const;
	bool intersects(const cocos2d::Vec2& location) const;

	EntityID getId() const;
	EntityActionType getActionType() const;
	EntityActionType getDefaultActionType() const;
	bool isActionTypeSupported(EntityActionType type) const;

	int getTeamID() const 
	{
		return _teamID;
	}

	bool isHostileTo(const Entity* other) const;
	bool isAllyOf(const Entity* other) const;

	virtual void executeAction(const InputContext* inputContext);
	virtual void stopAction();
	virtual bool canExecute(const InputContext* inputContext) const;

	virtual void onSelected(bool interactable = true);
	virtual void onDeselected();
	virtual void update(float dt) = 0;
	virtual void registerToWorld(World* world);
	virtual void unregisterFromWorld(World* world);

	bool isAdjacentTo(Entity* other) const;

	const std::map<InputActionKey, EntityActionType>& getActionMappings() const;

protected:
	World* _world;

	std::map<InputActionKey, EntityActionType> _actionMappings;
	int _instanceID;
	int _teamID;
	int _actionIdx;

	EntityActionType _currentActionType = EntityActionType::Undefined;

	virtual void initActionMappings();
	virtual void onSetup(const EntityLevelData& levelData) = 0;
	virtual void createView(cocos2d::Node* parent) = 0;
};