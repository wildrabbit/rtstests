#pragma once
#include "EntityTypes.h"

class EntityDatabase;
class World;
struct EntityLevelData;

namespace EntityFactory
{
	EntityPtr createEntity(World* world, int teamID, const EntityDatabase* database, const EntityLevelData& levelData);
}
