#pragma once
#include "EntityTypes.h"
#include "Entity.h"
#include <vector>

struct UnitData : public EntityData
{
	float moveSpeed;
	float spawnTime;
	float baseAttack;
	float baseDefense;
	float attackRate;
	UnitData(const EntityData& baseData, float _moveSpeed, float _spawnTime, float _baseAttack, float _baseDefense, float _attackRate);
};

enum class UnitState: unsigned char
{
	Idle = 0,
	Move,
	Attack,
	Hurt,
	Dying,
	Dead
};

class Unit : public Entity
{
	using VCoords = std::vector<IntVec2>;
	using UnitCallback = std::function<void(Unit*)>;
public:
	Unit(World*);
	~Unit() = default;

private:
	UnitState state = UnitState::Idle;

	// Motion
	IntVec2 nextCoords;
	cocos2d::Vec2 nextPos;
	IntVec2 tgtCoords;

	// Action placement
	IntVec2 actionTargetCoords;

	cocos2d::Vec2 velocity;
	cocos2d::Vec2 acceleration;

	UnitCallback _targetCallback = nullptr;
	const UnitData* unitData = nullptr;

public:
	void registerToWorld(World* world) override;
	void unregisterFromWorld(World* world)  override;

	void onSetup(const EntityLevelData& levelData) override;
	void createView(cocos2d::Node* parent) override;

	void update(float dt) override;	
};