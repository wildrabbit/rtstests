#include "Castle.h"

#include "../Map.h"
#include "../World.h"
#include "../WorldData.h"
#include "cocos2d.h"

USING_NS_CC;

const int kDefaultState = 0;
const int kDamagedState = 1;
const int kDestroyedState = 2;

CastleData::CastleData(const EntityData& baseData)
	:EntityData(baseData)
{}

Castle::Castle(World* world)
: Entity(world)
{
}

Castle::~Castle()
{
	const int numStates = kDestroyedState - kDefaultState + 1;

	for (int i = 0; i < numStates; ++i)
	{
		frames[i]->release();
	}
}

void Castle::onSetup(const EntityLevelData& levelData)
{
	const IntVec2 tileSize = _world->map()->getTileSize();
	const int numStates = kDestroyedState - kDefaultState + 1;
	float width = data->size.x * tileSize.x;
	float height = data->size.y * tileSize.y;

	for (int i = 0; i < numStates; ++i)
	{
		frames[i] = SpriteFrame::create(data->path, { i * width, 0.f, width, height });
		frames[i]->retain();
	}
	
	currentState = kDefaultState;
}

void Castle::createView(cocos2d::Node* parent)
{
	view = Sprite::createWithSpriteFrame(frames[currentState]);
	view->setAnchorPoint({ 0.f, 1.f });

	if (parent) parent->addChild(view, 5);
}


void Castle::setSelectionState(bool selected)
{
	Color3B colour = selected ? Color3B::YELLOW : Color3B::WHITE;
	view->setColor(colour);
}

void Castle::update(float dt)
{
}

void Castle::onSelected(bool interactable)
{
	setSelectionState(interactable);
}
void Castle::onDeselected()
{
	setSelectionState(false);
}
