#include "EntityFactory.h"
#include "Entity.h"
#include "../World.h"
#include "../WorldData.h"
#include "../../database/EntityDatabase.h"
#include "../../GameOptions.h"

namespace EntityFactory
{
	EntityPtr createEntity(World* world, int teamID, const EntityDatabase* database, const EntityLevelData& levelData)
	{
		const EntityData* data = database->getEntityData(levelData.id);
		if (!data)
		{
			return nullptr;
		}
		
		EntityPtr entityPtr;
		switch (data->typeID)
		{
			case EntityType::Builder:
			{
				entityPtr = std::make_shared<Builder>(world);
				break;
			}
			case EntityType::Building:
			{
				entityPtr = std::make_shared<Building>(world);
				break;
			}
			case EntityType::Castle:
			{
				entityPtr = std::make_shared<Castle>(world);
				break;
			}
			case EntityType::Warrior:
			{
				entityPtr = std::make_shared<Unit>(world);
				break;
			}
		}
		entityPtr->setup(data, levelData, teamID, world->root());
		return entityPtr;
	}
}