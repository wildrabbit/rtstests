#include "World.h"
#include "Map.h"
#include "Team.h"
#include "WorldData.h"
#include "WorldUtils.h"

#include "../database/EntityDatabase.h"
#include "../GameOptions.h"
#include "entities/EntityFactory.h"
#include "cocos/cocos2d.h"
#include <algorithm>

#define REMOVE_ELEM(vector, elem) vector.erase(std::remove(vector.begin(), vector.end(), elem), vector.end());

USING_NS_CC;

World::World(const EntityDatabase* database, const GameOptions* options, cocos2d::Node* root)
	:_database(database)
	,_options(options)
	,_root(root)
{	
}

Team* ::World::getTeamById(TeamID teamID)
{
	MTeams::iterator teamIter = _teams.find(teamID);
	if (teamIter == _teams.end())
	{
		return nullptr;
	}
	return &teamIter->second;
}

std::unique_ptr<World> World::create(const EntityDatabase* database, const GameOptions* options, cocos2d::Node* root)
{
	return std::make_unique<World>(database, options, root);
}

bool World::loadLevel(const WorldLevelData& levelData)
{
	// Load map
	_map = std::make_unique<GameMap>();
	if (!_map->setup(levelData.mapData, _root))
	{
		// error
		return false;
	}

	for (const TeamLevelData& teamData : levelData.teams)
	{
		Team t(this);
		t.setup(teamData.id, teamData);
		_teams[teamData.id] = t;

		// Create instances
		for (const EntityLevelData& entityData : teamData.entities)
		{
			EntityPtr entity = EntityFactory::createEntity(this, teamData.id, _database, entityData);
			entity->registerToWorld(this);
		}
	}

	return true;
}

void World::spawnEntity(EntityID id, const IntVec2& coords, TeamID teamID)
{
	EntityLevelData instanceData;
	instanceData.id = id;
	instanceData.startPos = coords;
	EntityPtr entity = EntityFactory::createEntity(this, teamID, _database, instanceData);
	entity->registerToWorld(this);
}

void World::startLevel()
{
}

void World::update(float dt)
{
	for (EntityPtr entity : _entities)
	{
		entity->update(dt);
	}

	_entitiesToRemove.erase(_entitiesToRemove.begin(), _entitiesToRemove.end());
	_entitiesToRemove.clear();

	for (EntityPtr toAdd : _entitiesToAdd)
	{
		_entities.push_back(toAdd);
	}
	_entitiesToAdd.clear();
}

bool World::isOcuppiedTile(const IntVec2& coordsVec)
{
	for (EntityPtr e : _entities)
	{
		if (e->getMapBounds().contains(coordsVec.x, coordsVec.y))
		{
			return true;
		}
	}
	return false;
}

bool World::isBuildableArea(const IntRect& area)
{
	if (!_map->isBuildableArea(area))
	{
		return false;
	}
	
	return noEntitiesInArea(area);
}

bool World::noEntitiesInArea(const IntRect& area)
{
	for (EntityPtr e : _entities)
	{
		if (e->getMapBounds().intersects(area))
		{
			return false;
		}
	}
	return true;
}

bool World::isWalkableTile(const IntVec2& coordsVec)
{
	return _map->isWalkableTile(coordsVec);
}

Team* World::getTeamForBuilder(Builder* b)
{
	return nullptr;
}

void World::addEntity(EntityPtr entity)
{
	_entitiesToAdd.push_back(entity);
}

void World::removeEntity(EntityPtr entity)
{
	_entitiesToRemove.push_back(entity);	
}

void World::addUnit(UnitPtr unit)
{
	_units.push_back(unit);
}

void World::removeUnit(UnitPtr unit)
{
	auto iter = std::remove(_units.begin(), _units.end(), unit);
	_units.erase(iter, _units.end());
}

Entity* World::getFirstEntityAt(const cocos2d::Vec2& viewPos)
{
	auto entityIter = std::find_if(_entities.begin(), _entities.end(), [viewPos](const EntityPtr& entity) 
	{
		return entity->view->getBoundingBox().containsPoint(viewPos);
	});
	if (entityIter != _entities.end())
	{
		return entityIter->get();
	}
	return nullptr;
}

IntRect World::getBuildingAreaAtPos(const cocos2d::Vec2& screenPos, EntityID buildingID) const
{
	IntVec2 mapCoords = worldutils::screenToMapCoords(_map->getTileSize(), screenPos);
	return getBuildingAreaAtCoords(mapCoords, buildingID);
}

IntRect World::getBuildingAreaAtCoords(const IntVec2& mapCoords, EntityID buildingID) const
{
	const BuildingData* buildingData = _database->getBuildingData(buildingID);
	return buildingData ? IntRect{ mapCoords.x, mapCoords.y, buildingData->size.x, buildingData->size.y } : IntRect::ZERO;
}

void World::addEntityExecutionListener(IEntityActionExecutionListener* listener)
{
	_entityActionListeners.push_back(listener);
}

void World::removeEntityExecutionListener(IEntityActionExecutionListener* listener)
{
	REMOVE_ELEM(_entityActionListeners, listener);
}

void World::clearEntityExecutionListeners()
{
	_entityActionListeners.clear();
}

void World::onEntityBeganAction(Entity* e)
{
	for (IEntityActionExecutionListener* listener : _entityActionListeners)
	{
		listener->onExecutionBegan(e);
	}
}

void World::onEntityFinishedAction(Entity* e)
{
	for (IEntityActionExecutionListener* listener : _entityActionListeners)
	{
		listener->onExecutionFinished(e);
	}
}

void World::onEntityActionAborted(Entity* e)
{
	for (IEntityActionExecutionListener* listener : _entityActionListeners)
	{
		listener->onExecutionInterrupted(e);
	}
}

void World::onEntityCustomActionExecutionEvent(Entity* e, CustomEntityActionExecutionData* data)
{
	for (IEntityActionExecutionListener* listener : _entityActionListeners)
	{
		listener->onCustom(e, data);
	}
}

cocos2d::Sprite* World::placeBlueprint(EntityID id, const IntVec2& coords, TeamID teamID)
{
	const EntityData* data = _database->getEntityData(id);
	if (!data)
	{
		return nullptr;
	}

	IntVec2 tileSize = _map->getTileSize();
	const float widthInPixels = (float)(tileSize.x * data->size.x);
	const float heightInPixels = (float)(tileSize.y * data->size.y);
	cocos2d::Sprite* sp = cocos2d::Sprite::createWithSpriteFrame(SpriteFrame::create(data->path, { widthInPixels, 0.f, widthInPixels, heightInPixels}));
	sp->setOpacity(0x80);
	sp->setAnchorPoint({ 0.f, 1.f });
	_root->addChild(sp);
	cocos2d::Vec2 bpPos = worldutils::coordsToWorldPos(coords, this);
	sp->setPosition(bpPos);

	_buildingBlueprints.insert(sp);
	return sp;
}

void World::removeBlueprint(cocos2d::Sprite* blueprint)
{
	blueprint->removeFromParent();
	_buildingBlueprints.erase(blueprint);
}