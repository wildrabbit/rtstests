#include "WorldUtils.h"
#include "World.h"
#include "Map.h"
#include "../GameOptions.h"
#include "cocos2d.h"
USING_NS_CC;

namespace worldutils
{
	cocos2d::Vec2 adjustScreenPosToView(const cocos2d::Vec2& screenPos, World* world)
	{
		Vec2 worldLocation = screenPos;
		IntVec2 screenSize = world->options()->winSize;
		worldLocation.y = screenSize.y - worldLocation.y;
		return worldLocation;
	}

	IntVec2 screenToMapCoords(const IntVec2& mapTileSize, const cocos2d::Vec2& screenPos)
	{
		IntVec2 mapCoords;
		mapCoords.y = (int)screenPos.y / mapTileSize.y;
		mapCoords.x = (int)screenPos.x / mapTileSize.x;
		return mapCoords;
	}

	cocos2d::Vec2 coordsToWorldPos(const IntVec2& coords, World* world)
	{
		const IntVec2 tileSize = world->map()->getTileSize();
		const IntVec2 winSize = world->options()->winSize;

		cocos2d::Vec2 pos(coords.x * tileSize.x, winSize.y - coords.y * tileSize.y);
		return pos;
	}


	IntVec2 worldPosToCoords(const cocos2d::Vec2& pos, World* world)
	{
		const IntVec2 mapTileSize = world->map()->getTileSize();
		cocos2d::Vec2 translatedPos = pos;
		translatedPos.y = world->options()->winSize.y - pos.y;
		
		IntVec2 mapCoords;
		mapCoords.y = (int)translatedPos.y / mapTileSize.y;
		mapCoords.x = (int)translatedPos.x / mapTileSize.x;
		return mapCoords;
	}
}
