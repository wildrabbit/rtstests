#pragma once
#include "cocos/math/CCGeometry.h"
#include "MathTypes.h"
class World;

namespace worldutils
{
	// mouse coords are given from the top left. Cocos uses bottom left :/
	cocos2d::Vec2 adjustScreenPosToView(const cocos2d::Vec2& screenPos, World* _world);
	IntVec2 screenToMapCoords(const IntVec2& mapTileSize, const cocos2d::Vec2& screenPos);	
	cocos2d::Vec2 coordsToWorldPos(const IntVec2& coords, World* _world);
	IntVec2 worldPosToCoords(const cocos2d::Vec2& pos, World* world);
}
