#pragma once

#include "entities/EntityTypes.h"
#include "TeamTypes.h"
#include "../MathTypes.h"
#include <vector>
#include "entityActions/EntityActionExecutionListener.h"
#include <map>
#include <vector>
#include <set>

class GameMap;
using MapPtr = std::unique_ptr<GameMap>;
using VEntityActionExecutionListeners = std::vector<IEntityActionExecutionListener*>;

namespace cocos2d
{
	class Node;
	class Vec2;
	class Sprite;
};

class EntityDatabase;
struct GameOptions;
struct WorldLevelData;
class Team;


using MTeams = std::map<TeamID, Team>;
class World
{
public:
	World(const EntityDatabase* database, const GameOptions* options, cocos2d::Node* root);
	~World() = default;

	static std::unique_ptr<World> create(const EntityDatabase* database, const GameOptions* options, cocos2d::Node*);
	bool loadLevel(const WorldLevelData& levelData);
	void startLevel();
	void update(float dt);

	const GameOptions* options() const {
		return _options;
	}

	const GameMap* map() const {
		return _map.get();
	}

	cocos2d::Node* root()
	{
		return _root;
	}

	Entity* getFirstEntityAt(const cocos2d::Vec2& coords);

	bool isOcuppiedTile(const IntVec2& coordsVec);
	bool isBuildableArea(const IntRect& area);
	bool isWalkableTile(const IntVec2& coordsVec);
	bool noEntitiesInArea(const IntRect& area);

	IntRect getBuildingAreaAtPos(const cocos2d::Vec2& screenPos, EntityID buildingID) const;
	IntRect getBuildingAreaAtCoords(const IntVec2& mapCoords, EntityID buildingID) const;

	Team* getTeamForBuilder(Builder* b);
	Team* getTeamById(TeamID teamID);

	void spawnEntity(EntityID id, const IntVec2& coords, TeamID teamID);

	void addEntity(EntityPtr);
	void removeEntity(EntityPtr);
	void addUnit(UnitPtr);
	void removeUnit(UnitPtr);

	void addEntityExecutionListener(IEntityActionExecutionListener* listener);
	void removeEntityExecutionListener(IEntityActionExecutionListener* listener);
	void clearEntityExecutionListeners();

	void onEntityBeganAction(Entity* e);
	void onEntityFinishedAction(Entity* e);
	void onEntityActionAborted(Entity* e);
	void onEntityCustomActionExecutionEvent(Entity* e, CustomEntityActionExecutionData* data);

	cocos2d::Sprite* placeBlueprint(EntityID id, const IntVec2& coords, TeamID teamID);
	void removeBlueprint(cocos2d::Sprite* blueprint);

private:
	MTeams _teams;

	VEntityActionExecutionListeners _entityActionListeners;

	std::vector<EntityPtr> _entitiesToAdd;
	std::vector<EntityPtr> _entitiesToRemove;

	std::vector<EntityPtr> _entities;
	std::vector<BuildingPtr> _buildings;
	std::vector<UnitPtr> _units;
	MapPtr _map;

	const EntityDatabase* _database;
	const GameOptions* _options;
	cocos2d::Node* _root = nullptr;

	std::set<cocos2d::Sprite*> _buildingBlueprints;
};
