#include "EntityDatabase.h"

EntityDatabase::EntityDatabase()
{
}
EntityDatabase::~EntityDatabase()
{
}

void EntityDatabase::init(const CastleData& castle, const BuilderData& builder, const std::vector<BuildingData>& buildings, const std::vector<UnitData>& unitData)
{
	_castle = castle;
	_builder = builder;
	initBuildings(buildings);
	initUnits(unitData);
}

const CastleData* EntityDatabase::getCastleData() const
{
	return &_castle;
}

const BuilderData* EntityDatabase::getBuilderData() const
{
	return &_builder;
}

const EntityData* EntityDatabase::getEntityData(EntityID id) const
{
	if (id == _castle.ID)
	{
		return &_castle;
	}
	else if (id == _builder.ID)
	{
		return &_builder;
	}
	auto building = getBuildingData(id);
	if (building) return building;

	auto unit = getUnitData(id);
	if (unit) return unit;

	return nullptr;
}

void EntityDatabase::initBuildings(const std::vector<BuildingData>& buildings)
{
	_buildings.clear();
	for (const BuildingData& item : buildings)
	{
		_buildings[item.ID] = std::move(item);
	}
}

void EntityDatabase::initUnits(const std::vector<UnitData>& units)
{
	_units.clear();
	for (auto item : units)
	{
		_units.emplace(item.ID, item);
	}
}

const BuildingData* EntityDatabase::getBuildingData(EntityID id) const
{
	auto buildingIter = _buildings.find(id);
	return buildingIter == _buildings.end() ? nullptr : &(buildingIter->second);
}

const UnitData* EntityDatabase::getUnitData(EntityID id) const
{
	auto unitIter = _units.find(id);
	return unitIter == _units.end() ? nullptr : &(unitIter->second);
}