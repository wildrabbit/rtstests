#pragma once

#include <map>
#include "../world/entities/EntityTypes.h"
#include "../world/entities/Castle.h"
#include "../world/entities/Builder.h"
#include "../world/entities/Building.h"
#include "../world/entities/Unit.h"

typedef std::map<EntityID, BuildingData> BuildingsMap;
typedef std::map<EntityID, UnitData> UnitsMap;

class EntityDatabase
{
public:
	EntityDatabase();
	~EntityDatabase();

	void init(const CastleData& castle, const BuilderData& builder, const std::vector<BuildingData>& buildings, const std::vector<UnitData>& units);

	const BuildingData* getBuildingData(EntityID id) const;
	const UnitData* getUnitData(EntityID id) const;
	const CastleData* getCastleData() const;
	const BuilderData* getBuilderData() const;

	const EntityData* getEntityData(EntityID id) const;
private:
	CastleData _castle;
	BuilderData _builder;
	BuildingsMap _buildings;
	UnitsMap _units;

	void initBuildings(const std::vector<BuildingData>& buildings);
	void initUnits(const std::vector<UnitData>& units);
};
