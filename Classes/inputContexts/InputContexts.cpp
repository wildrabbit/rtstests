#include "inputContexts/InputContexts.h"
#include "../world/entities/Entity.h"
#include "../PlayerController.h"

InputContext::InputContext(PlayerController* p, World* w)
	:p(p), w(w)
{
}
void InputContext::onClick(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType)
{
	Entity* e = w->getFirstEntityAt(viewPos);
	if (e == nullptr) return;
	
	if (pointerType == InputPointerType::Main)
	{
		p->selectEntity(e);
		p->switchSelectionContext(InputContextTypes::kEntitySelection);
	}
}
void InputContext::onDragStart(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType)
{
}
void InputContext::onDragUpdate(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType, float dt)
{
}
void InputContext::onDragEnd(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType)
{
}

void InputContext::onActionKey(InputActionKey actionKey)
{
}

bool InputContext::ready() const
{
	return true;
}

void InputContext::reset()
{
}

void InputContext::activate(bool reactivate)
{
}

void InputContext::execute()
{
}

//---------------
EntitySelectionContext::EntitySelectionContext(PlayerController* p, World* w)
	:InputContext(p, w)
{
}
void EntitySelectionContext::onClick(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType)
{
	Entity* selected = p->getSelectedEntity();

	if (selected == nullptr)
	{
		// This should never happen!
		return;
	}

	Entity* e = w->getFirstEntityAt(viewPos);
	const bool clickOnEntity = e != nullptr;
	if (clickOnEntity)
	{
		const bool isMainPointer = pointerType == InputPointerType::Main;
		const bool isSelectedEntity = e == selected;
		const bool isRivalEntity = e->getTeamID() != p->getBoundTeamID();

		if (isMainPointer)
		{
			if (!isSelectedEntity)
			{
				p->selectEntity(e);
			}
		}
		else
		{
			// Depending on the entity's currently selected action we can filter and set the action target
			if (actionID == EntityActionType::MoveTo || actionID == EntityActionType::MoveAttackTarget || actionID == EntityActionType::HealRepairEntity)
			{
				if (selected->isHostileTo(e))
				{
					actionID = EntityActionType::MoveAttackTarget;
				}
				else if (selected->isAllyOf(e))
				{
					actionID = EntityActionType::HealRepairEntity;
				}
			}
			if (actionID == EntityActionType::MoveAttackTarget || actionID == EntityActionType::HealRepairEntity)
			{
				actionParamEntity = e;
			}
		}
	}
	else // Click on area
	{
		if (pointerType == InputPointerType::Main)
		{
			p->deselectEntity();
			p->switchSelectionContext(InputContextTypes::kDefault);
		}
		else // Apply params to action if available
		{
			if (actionID == EntityActionType::MoveTo || actionID == EntityActionType::MoveAttackTarget || actionID == EntityActionType::HealRepairEntity || actionID == EntityActionType::ConstructBuilding)
			{
				actionParamCoords = mapCoords;
				if (actionID != EntityActionType::ConstructBuilding)
				{
					actionID = EntityActionType::MoveTo;
				}
			}
		}
	}
}
void EntitySelectionContext::onDragStart(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType)
{
}
void EntitySelectionContext::onDragUpdate(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType, float dt)
{
}
void EntitySelectionContext::onDragEnd(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType)
{
}

bool EntitySelectionContext::ready() const
{
	if (entity == nullptr || entity->getTeamID() != p->getBoundTeamID() || actionID == EntityActionType::Undefined)
	{
		return false;
	}	

	return entity->canExecute(this);
}

void EntitySelectionContext::reset()
{
	entity = nullptr;
	actionID = EntityActionType::Undefined;
	actionParamCoords = { -1,-1 };
	actionParamPos = { -1.f, -1.f };
	actionParamEntity = nullptr;
}

void EntitySelectionContext::activate(bool reactivate )
{
	actionParamCoords = { -1,-1 };
	actionParamPos = { -1.f, -1.f };
	actionParamEntity = nullptr;
	if (entity)
	{
		actionID = entity->getDefaultActionType();
	}

	if (!reactivate)
	{
		entity = p->getSelectedEntity();
		actionID = entity->getDefaultActionType();
	}
	else
	{
		p->dispatchContextAction(ContextActionType::Update, this);
	}	
}

void EntitySelectionContext::onActionKey(InputActionKey actionKey)
{
	const std::map<InputActionKey, EntityActionType>& actionMappings = entity->getActionMappings();
	if (entity == nullptr) return;
	bool updated = false;
	
	auto actionMapping = actionMappings.find(actionKey);
	
	if (actionMapping != actionMappings.end() && entity->isActionTypeSupported(actionMapping->second))
	{
		updated = true;
		actionID = actionMapping->second;
	}

	if (updated)
	{
		p->dispatchContextAction(ContextActionType::Update, this);
	}
}

void EntitySelectionContext::execute()
{
	entity->executeAction(this); // TODO: Translate context!
	activate(true);	
}

//---------------
AreaSelectionContext::AreaSelectionContext(PlayerController* p, World* w)
:InputContext(p, w)
{
}

void AreaSelectionContext::onClick(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType)
{
}
void AreaSelectionContext::onDragStart(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType)
{
}
void AreaSelectionContext::onDragUpdate(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType, float dt)
{
}
void AreaSelectionContext::onDragEnd(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType)
{
}

void AreaSelectionContext::onActionKey(InputActionKey actionKey)
{
}

bool AreaSelectionContext::ready() const
{
	return entities.size() >= 0 && !actionID.empty(); // && action params	
}

void AreaSelectionContext::reset()
{
	entities.clear();
	actionID.clear();
}

void AreaSelectionContext::activate(bool reactivate)
{
}

void AreaSelectionContext::execute()
{
}