#pragma once
using InputContextType = int;

namespace InputContextTypes
{
	const InputContextType kDefault = 0;
	const InputContextType kFirst = kDefault;

	const InputContextType kEntitySelection = 1;
	const InputContextType kAreaSelection = 2;
	const InputContextType kLast = kAreaSelection;
}

constexpr int kNumContexts = InputContextTypes::kLast - InputContextTypes::kFirst + 1;

enum class InputPointerType
{
	Main,
	Auxiliary
};

enum class InputActionKey
{
	Build,
	Move,
	Repair,
	MoveAttack,
	Cancel,
	GameMenu,
	RequestBuild, //For units
};


enum class ContextActionType : int
{
	Switch,
	Update
};

struct InputContext;