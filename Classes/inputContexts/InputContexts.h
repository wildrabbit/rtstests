#pragma once

#include "InputContextDefs.h"
#include "../world/World.h"
#include "../world/entities/EntityTypes.h"
#include "cocos/math/CCGeometry.h"
#include <string>
#include <vector>

class PlayerController;



struct InputContext
{
	PlayerController* p;
	World* w;

	InputContext(PlayerController* p, World* w);

	virtual bool ready() const;
	virtual ~InputContext() = default;
	virtual void reset();
	virtual void activate(bool reactivate = false);
	virtual void execute();
	
	virtual void onClick(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType);
	virtual void onDragStart(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType);
	virtual void onDragUpdate(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType, float dt);
	virtual void onDragEnd(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType);
	virtual void onActionKey(InputActionKey actionKey);
};

struct EntitySelectionContext : public InputContext
{
	EntitySelectionContext(PlayerController* p, World* w);

	Entity* entity = nullptr;
	EntityActionType actionID = EntityActionType::Undefined;
	
	IntVec2 actionParamCoords = { -1,-1 };
	cocos2d::Vec2 actionParamPos = { -1.f, -1.f };
	Entity* actionParamEntity = nullptr;

	virtual void reset() override;
	virtual void activate(bool reactivate = false) override;
	virtual bool ready() const override;
	virtual void execute() override;

	virtual void onClick(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType) override;
	virtual void onDragStart(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType) override;
	virtual void onDragUpdate(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType, float dt) override;
	virtual void onDragEnd(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType) override;
	virtual void onActionKey(InputActionKey actionKey) override;
};

struct AreaSelectionContext : public InputContext
{
	AreaSelectionContext(PlayerController* p, World* w);
	std::vector<EntityID> entities;
	std::string actionID;
	void reset() override;
	bool ready() const override;
	void activate(bool reactivate = false) override;
	void execute() override;

	virtual void onClick(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType) override;
	virtual void onDragStart(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType) override;
	virtual void onDragUpdate(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType, float dt) override;
	virtual void onDragEnd(const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, InputPointerType pointerType) override;
	virtual void onActionKey(InputActionKey actionKey) override;
};