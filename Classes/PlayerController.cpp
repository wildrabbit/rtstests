#include "PlayerController.h"

#include "cocos2d.h"
#include "input/GameInput.h"
#include "inputContexts/InputContexts.h"
#include "world/Map.h"
#include "world/Team.h"
#include "world/World.h"
#include "world/WorldUtils.h"
#include "world/entities/Entity.h"


USING_NS_CC;

PlayerCallbackID PlayerController::_callbackID = 0;
ContextActionCallbackID PlayerController::_contextActionCallbackID;

std::unique_ptr<PlayerController> PlayerController::create(World* world)
{
	return std::make_unique<PlayerController>(world);
}

PlayerController::PlayerController(World* world)
:_world(world)
{
	_inputContexts.push_back(std::make_unique<InputContext>(this, _world));
	_inputContexts.push_back(std::make_unique<EntitySelectionContext>(this, _world));
	_inputContexts.push_back(std::make_unique<AreaSelectionContext>(this, _world));

	_pointerTypeMappings = { {EventMouse::MouseButton::BUTTON_LEFT, InputPointerType::Main}, {EventMouse::MouseButton::BUTTON_RIGHT, InputPointerType::Auxiliary} };
	_actionKeyMappings = {
		{ EventKeyboard::KeyCode::KEY_B, InputActionKey::Build},
		{ EventKeyboard::KeyCode::KEY_R, InputActionKey::Repair},
		{ EventKeyboard::KeyCode::KEY_M, InputActionKey::Move},
		{ EventKeyboard::KeyCode::KEY_A, InputActionKey::MoveAttack},
		{ EventKeyboard::KeyCode::KEY_X, InputActionKey::Cancel},
		{ EventKeyboard::KeyCode::KEY_1, InputActionKey::RequestBuild},
	};

	for (auto actionKey : _actionKeyMappings)
	{
		_keycodeStates[actionKey.first] = { false, false };
	}
}


PlayerController::~PlayerController()
{
}

void PlayerController::bindToTeam(int teamID)
{
	_team = _world->getTeamById(teamID);
}


void PlayerController::update(float dt, const GameInput* input)
{
	IntVec2 mouseMapCoords = worldutils::screenToMapCoords(_world->map()->getTileSize(), input->mousePos);
	cocos2d::Vec2 viewPos = worldutils::adjustScreenPosToView(input->mousePos, _world);

	InputContext* context = _inputContexts[_activeInputContextIdx].get();

	processPointerEntry(input, EventMouse::MouseButton::BUTTON_LEFT, context, viewPos, mouseMapCoords, dt, _mainWasDown, _mainDown);
	processPointerEntry(input, EventMouse::MouseButton::BUTTON_RIGHT, context, viewPos, mouseMapCoords, dt, _auxWasDown, _auxDown);
	processKeyRelease(input, context);

	if (context->ready())
	{
		context->execute();
	}
}

void PlayerController::processKeyRelease(const GameInput* input, InputContext* context)
{
	for (MKeyCodeStates::value_type& keyState: _keycodeStates)
	{
		keyState.second.wasDown = keyState.second.down;
		const bool isKeyPressed = input->isKeyPressed(keyState.first);
		keyState.second.down = isKeyPressed;

		bool isUp = keyState.second.wasDown && !keyState.second.down;
		if (isUp)
		{
			context->onActionKey(_actionKeyMappings[keyState.first]);
		}
	}
}

void PlayerController::processPointerEntry(const GameInput* input, EventMouse::MouseButton buttonType, InputContext* context, const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, float dt, bool& oldValue, bool& currentValue)
{
	oldValue = currentValue;
	currentValue = input->isMousePressed(buttonType);
	InputPointerType pointerType = _pointerTypeMappings[buttonType];

	if (currentValue)
	{
		if (oldValue)
		{
			const double kDragThreshold = 0.25;
			double duration = input->mousePressDuration(buttonType);
			if (duration > 0.000001f)
			{
				if (_dragArea.equals(Rect::ZERO))
				{
					context->onDragStart(viewPos, mapCoords, pointerType);
				}				
				else
				{
					context->onDragUpdate(viewPos, mapCoords, pointerType, dt);
				}
			}
		}
	}
	else
	{
		if (oldValue)
		{
			// Mouse up! If we have an area, process area
			if (!_dragArea.equals(Rect::ZERO))
			{
				context->onDragEnd(viewPos, mapCoords, pointerType);
			}
			else
			{
				context->onClick(viewPos, mapCoords, pointerType);
			}
		}
	}
}


void PlayerController::switchSelectionContext(InputContextType type)
{
	if (type != _activeInputContextIdx)
	{
		_inputContexts[_activeInputContextIdx]->reset();
	}
	_activeInputContextIdx = type;
	_inputContexts[_activeInputContextIdx]->activate();
	dispatchContextAction(ContextActionType::Switch, _inputContexts[_activeInputContextIdx].get());
}

void PlayerController::selectEntity(Entity* entity)
{
	if (_singleEntity != nullptr)
	{
		deselectEntity();
	}
	deselectGroup();

	if (entity != nullptr)
	{
		_singleEntity = entity;
		bool interactable = entity->getTeamID() == _team->id();
		_singleEntity->onSelected(interactable);
		for (auto cb : _selectionCallbacks)
		{
			cb.second(true, entity, interactable);
		}
	}
}

void PlayerController::deselectEntity()
{
	if (_singleEntity)
	{
		_singleEntity->onDeselected();
	}
	for (auto cb : _selectionCallbacks)
	{
		cb.second(false, _singleEntity, false);
	}

	_singleEntity = nullptr;
}

void PlayerController::selectGroup(const VEntities& group)
{
	if (_singleEntity)
	{
		deselectEntity();
	}
	deselectGroup();

	_selectedEntities = std::move(group);
	for (auto entity : _selectedEntities)
	{
		entity->onSelected();
	}
}

void PlayerController::deselectGroup()
{
	for (auto entity : _selectedEntities)
	{
		entity->onDeselected();
	}
}

Entity* PlayerController::getSelectedEntity() const
{
	return _singleEntity;
}

void PlayerController::clearSelectionCallbacks()
{
	_selectionCallbacks.clear();
	// reset id??
}

PlayerCallbackID PlayerController::addSelectionCallback(PlayerSelectionCallback cb)
{
	int newID = _callbackID++;
	_selectionCallbacks[newID] = cb;
	_callbackID++;
	return newID;
}
void PlayerController::removeSelectionCallback(PlayerCallbackID id)
{
	_selectionCallbacks.erase(id);
}

void PlayerController::clearContextActionCallbacks()
{
	_contextActionCallbacks.clear();
}
ContextActionCallbackID PlayerController::addContextActionCallback(ContextActionCallback cb)
{
	int newID = _contextActionCallbackID++;
	_contextActionCallbacks[newID] = cb;
	_contextActionCallbackID++;
	return newID;
}

void PlayerController::removeContextActionCallback(ContextActionCallbackID id)
{
	_contextActionCallbacks.erase(id);
}

void PlayerController::dispatchContextAction(ContextActionType type, InputContext* ctxt)
{
	for (auto callbackPair : _contextActionCallbacks)
	{
		callbackPair.second(type, ctxt);
	}
}

int PlayerController::getBoundTeamID() const
{
	return _team->id();
}