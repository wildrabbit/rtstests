#pragma once

#include "cocos/math/CCGeometry.h"
#include "cocos/base/CCEventMouse.h"
#include "cocos/base/CCEventKeyboard.h"
#include "MathTypes.h"
#include "inputContexts/InputContextDefs.h"
#include "world/entities/EntityTypes.h"
#include <string>

class GameInput;
class Team;
class World;
class Entity;

struct InputContext;
using InputContextPtr = std::unique_ptr<InputContext>;
using VSelectionContexts = std::vector<InputContextPtr>;

using KeyCode = cocos2d::EventKeyboard::KeyCode;
using MouseButton = cocos2d::EventMouse::MouseButton;

using MInputPointerTypeMappings = std::map<MouseButton, InputPointerType>;
using MInputActionKeyMappings = std::map<KeyCode, InputActionKey>;
using VEntities = std::vector<Entity*>;

using PlayerCallbackID = int;
using PlayerSelectionCallback = std::function<void(bool, Entity*, bool)>;
using MPlayerSelectionCallbacks = std::map<PlayerCallbackID, PlayerSelectionCallback>;

struct KeyCodeTracker
{
	bool down = false;
	bool wasDown = false;
};
using MKeyCodeStates = std::map<KeyCode, KeyCodeTracker>;


using ContextActionCallbackID = int;
using ContextActionCallback = std::function<void(ContextActionType, const InputContext*)>;
using MContextActionCallbacks = std::map<ContextActionCallbackID, ContextActionCallback>;

class PlayerController
{
public:
	static std::unique_ptr<PlayerController> create(World* world);
	PlayerController(World* world);
	~PlayerController();
	void bindToTeam(int teamID);
	void update(float dt, const GameInput* input);
	void switchSelectionContext(InputContextType type);

	void selectEntity(Entity* entity);
	void deselectEntity();

	void selectGroup(const VEntities& group);
	void deselectGroup();

	Entity* getSelectedEntity() const;

	int getBoundTeamID() const;

	void clearSelectionCallbacks();
	PlayerCallbackID addSelectionCallback(PlayerSelectionCallback cb);
	void removeSelectionCallback(PlayerCallbackID);

	void clearContextActionCallbacks();
	ContextActionCallbackID addContextActionCallback(ContextActionCallback cb);
	void removeContextActionCallback(ContextActionCallbackID);

	void dispatchContextAction(ContextActionType type, InputContext* ctxt);

	//void clearAreaSelectionCalbacks();
	//void addAreaSelectionCallback(PlayerAreaSelectionCallback cb);
	//void removeAreaSelectionCallback(PlayerAreaSelectionCallback cb); // will this work?


private:
	static PlayerCallbackID _callbackID;
	static ContextActionCallbackID _contextActionCallbackID;
	//static PlayerCallbackID _areaCallbackID = 0;
	void processPointerEntry(const GameInput* input, cocos2d::EventMouse::MouseButton buttonType, InputContext* selectionContext, const cocos2d::Vec2& viewPos, const IntVec2& mapCoords, float dt, bool& oldValue, bool& currentValue);
	void processKeyRelease(const GameInput* input, InputContext* selectionContext);

	Team* _team = nullptr;
	World* _world = nullptr;
	Entity* _singleEntity = nullptr;
	VEntities _selectedEntities;

	bool _mainWasDown = false;
	bool _mainDown = false;

	bool _auxWasDown = false;
	bool _auxDown = false;
	
	cocos2d::Rect _dragArea;
	IntRect _mapDragCoords;

	VSelectionContexts _inputContexts;
	InputContextType _activeInputContextIdx = InputContextTypes::kDefault;
	MInputPointerTypeMappings _pointerTypeMappings;
	MInputActionKeyMappings _actionKeyMappings;
	MKeyCodeStates _keycodeStates;

	//VPlayerAreaSelectionCallback _areaSelectionCallbacks;
	MPlayerSelectionCallbacks _selectionCallbacks;
	MContextActionCallbacks _contextActionCallbacks;
};
