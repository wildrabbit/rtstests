#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "database/EntityDatabase.h"
#include "GameOptions.h"
#include <memory>

class Hud;
class GameInput;
class PlayerController;// ??
class World;

class Team;
class GameMap;

typedef std::unique_ptr<Team> TeamPtr;
typedef std::unique_ptr<GameMap> MapPtr;
typedef std::unique_ptr<Hud> HudPtr;
typedef std::unique_ptr<GameInput> GameInputPtr;
typedef std::unique_ptr<World> WorldPtr;
typedef std::unique_ptr<PlayerController> PlayerControllerPtr;

struct IntVec2;

class CastleFight : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();

	// a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(CastleFight);


	void update(float dt);

	
private:
	void initDatabase();

	const BuildingData* _constructionData = nullptr;
	
	cocos2d::Vec2 _mousePos;
private:
	EntityDatabase _database;
	GameOptions _options;

	HudPtr _hud;
	GameInputPtr _input;
	WorldPtr _world;
	PlayerControllerPtr _playerController;
};

#endif // __HELLOWORLD_SCENE_H__
