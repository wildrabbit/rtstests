#pragma once

#include "../cocos/base/CCEventKeyboard.h"
#include "../cocos/base/CCEventMouse.h"
#include <memory>

namespace cocos2d
{
	class Node;
	class Touch;
	class Event;

	class EventMouse;
	class EventListenerMouse;
};

constexpr int MouseButtonCount = (int)cocos2d::EventMouse::MouseButton::BUTTON_MIDDLE - (int)cocos2d::EventMouse::MouseButton::BUTTON_LEFT + 1;
constexpr int KeyCodeCount = (int)cocos2d::EventKeyboard::KeyCode::KEY_PLAY - (int)cocos2d::EventKeyboard::KeyCode::KEY_NONE;

class GameInput
{
public:
	GameInput(cocos2d::Node* inputLayer);
	~GameInput() = default;
	using TimePoint = std::chrono::system_clock::time_point;
	static std::unique_ptr<GameInput> create(cocos2d::Node* inputLayer);
	// key array?

	bool init();

	void onMouseUp(cocos2d::EventMouse*);
	void onMouseDown(cocos2d::EventMouse*);
	void onMouseMove(cocos2d::EventMouse*);
	void onMouseScroll(cocos2d::EventMouse*);

	void onKeyPressed(cocos2d::EventKeyboard::KeyCode kc, cocos2d::Event* evt);
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode kc, cocos2d::Event* evt);

	cocos2d::Vec2 mousePos;
	float mouseScrollAmount;

	bool isMousePressed(cocos2d::EventMouse::MouseButton) const;
	double mousePressDuration(cocos2d::EventMouse::MouseButton) const;

	bool isKeyPressed(cocos2d::EventKeyboard::KeyCode) const;
	double keyPressedDuration(cocos2d::EventKeyboard::KeyCode) const;

private:
	TimePoint _mouseState[MouseButtonCount];
	TimePoint _keyState[KeyCodeCount];


	cocos2d::EventListenerKeyboard* _keyboardListener = nullptr;
	cocos2d::EventListenerMouse* _mouseListener = nullptr;

	cocos2d::Node* _inputLayer = nullptr;
};
