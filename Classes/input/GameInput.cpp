#include "GameInput.h"
#include "../cocos/cocos2d.h"
#include <tuple>
#include <chrono>

USING_NS_CC;

const GameInput::TimePoint kNoTime = std::chrono::system_clock::from_time_t(0);


std::unique_ptr<GameInput> GameInput::create(cocos2d::Node* inputLayer)
{
	return std::make_unique<GameInput>(inputLayer);
}
// key array?

const double kNoInput = -1.f;

GameInput::GameInput(cocos2d::Node* inputLayer)
: _inputLayer(inputLayer)
{
}

bool GameInput::init()
{
	for (int i = 0; i < MouseButtonCount; ++i)
	{
		_mouseState[i] = kNoTime;
	}

	const int firstKeyCode = (int)EventKeyboard::KeyCode::KEY_PAUSE;
	const int lastKeyCode = (int)EventKeyboard::KeyCode::KEY_PLAY;
	for (int i = firstKeyCode; i <= lastKeyCode; ++i)
	{
		_keyState[i - 1] = kNoTime;
	}

#if CC_TARGET_PLATFORM && CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	_mouseListener = EventListenerMouse::create();
	_mouseListener->onMouseUp = CC_CALLBACK_1(GameInput::onMouseUp, this);
	_mouseListener->onMouseDown = CC_CALLBACK_1(GameInput::onMouseDown, this);
	_mouseListener->onMouseMove = CC_CALLBACK_1(GameInput::onMouseMove, this);
	_mouseListener->onMouseScroll = CC_CALLBACK_1(GameInput::onMouseScroll, this);
#endif

	EventDispatcher* evtDispatcher = _inputLayer->getEventDispatcher();
	evtDispatcher->addEventListenerWithSceneGraphPriority(_mouseListener, _inputLayer);

	_keyboardListener = EventListenerKeyboard::create();
	_keyboardListener->onKeyPressed = CC_CALLBACK_2(GameInput::onKeyPressed, this);
	_keyboardListener->onKeyReleased = CC_CALLBACK_2(GameInput::onKeyReleased, this);
	evtDispatcher->addEventListenerWithSceneGraphPriority(_keyboardListener, _inputLayer);

	return true;
}

void GameInput::onMouseUp(cocos2d::EventMouse* evt)
{
	cocos2d::Vec2 location = evt->getLocation();
	mousePos = location;
	int buttonIdx = (int)evt->getMouseButton();
	_mouseState[buttonIdx] = kNoTime;
}
void GameInput::onMouseDown(cocos2d::EventMouse* evt)
{
	cocos2d::Vec2 location = evt->getLocation();
	mousePos = location;
	int buttonIdx = (int)evt->getMouseButton();
	_mouseState[buttonIdx] = std::chrono::system_clock::now();
}
void GameInput::onMouseMove(cocos2d::EventMouse* evt)
{
	cocos2d::Vec2 location = evt->getLocation();
	mousePos = location;
}

void GameInput::onMouseScroll(cocos2d::EventMouse* evt)
{
	cocos2d::Vec2 location = evt->getLocation();
	mousePos = location;

	this->mouseScrollAmount = evt->getScrollY();
}

void GameInput::onKeyPressed(cocos2d::EventKeyboard::KeyCode kc, cocos2d::Event* evt)
{
	_keyState[(int)kc - 1] = std::chrono::system_clock::now();
}
void GameInput::onKeyReleased(cocos2d::EventKeyboard::KeyCode kc, cocos2d::Event* evt)
{
	_keyState[(int)kc - 1] = kNoTime;
}

bool GameInput::isMousePressed(cocos2d::EventMouse::MouseButton btn) const
{
	int btnIdx = (int)btn;
	if (btnIdx < 0 || btnIdx >= MouseButtonCount)
	{
		// log "invalid button"
		return false;
	}
	return _mouseState[btnIdx] != kNoTime;
}
double GameInput::mousePressDuration(cocos2d::EventMouse::MouseButton btn) const
{
	if (!isMousePressed(btn))
	{
		return 0;
	}
	const int btnIdx = (int)btn;
	return 	std::chrono::duration_cast<std::chrono::milliseconds> (std::chrono::system_clock::now() - _mouseState[btnIdx]).count();
}

bool GameInput::isKeyPressed(cocos2d::EventKeyboard::KeyCode kc) const
{
	const int btnIdx = (int)kc - 1;
	if (btnIdx < 0 || btnIdx >= KeyCodeCount)
	{
		// log "invalid key"
		return false;
	}
	return _keyState[btnIdx] != kNoTime;
}

double GameInput::keyPressedDuration(cocos2d::EventKeyboard::KeyCode kc) const
{
	if (!isKeyPressed(kc))
	{
		return 0;
	}
	const int btnIdx = (int)kc;
	return std::chrono::duration_cast<std::chrono::milliseconds> (std::chrono::system_clock::now() - _keyState[btnIdx]).count();
}