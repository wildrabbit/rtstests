#include "CastleFightScene.h"

#include "PlayerController.h"

#include "input/GameInput.h"
#include "hud/Hud.h"

#include "world/World.h"
#include "world/entities/Castle.h"
#include "world/entities/Builder.h"
#include "world/entities/Building.h"
#include "world/entities/Unit.h"
#include "world/Team.h"
#include "world/Map.h"
#include "world/WorldData.h"

#include <functional>

USING_NS_CC;


Scene* CastleFight::createScene()
{
    return CastleFight::create();
}

// on "init" you need to initialize your instance
bool CastleFight::init()
{
    if ( !Scene::init() )
    {
        return false;
    }

	_input = std::move(GameInput::create(this));
	if (!_input->init())
	{
		return false;
	}

	initDatabase();
	const cocos2d::Size winSizeInPixels = Director::getInstance()->getWinSizeInPixels();
	_options.winSize.set((int)winSizeInPixels.width, (int)winSizeInPixels.height);
	
	_world = std::move(World::create(&_database, &_options, this));

	TeamLevelData team0 = { 0, true, { {0, {2, 10}}, {1, {1, 6}} }, {{1, TeamRelationshipType::Hostile}}};
	TeamLevelData team1 = { 1, false, { {0, {27, 10}}, {1, {28,6}} }, {{0, TeamRelationshipType::Hostile}}};

	MapLevelData map = { "testCF.tmx", { 32, 32 }, { 490, 491, 496, 497, 498, 536, 537, 542, 543, 544, 545 }, 0, "bg", "environment" };
	WorldLevelData levelData = { map, {team0, team1} };
	
	if (!_world->loadLevel(levelData))
	{
		return false;
	}

	_playerController = PlayerController::create(_world.get());
	_playerController->bindToTeam(0);

	_hud = Hud::create(this);
	if (!_hud->init(_world.get(), _input.get(), std::bind(&CastleFight::menuCloseCallback, this, std::placeholders::_1)))
	{
		return false;
	}

	_playerController->addContextActionCallback(std::bind(&Hud::onContextAction, _hud.get(), std::placeholders::_1, std::placeholders::_2));

	// GOGOGO!!
	// world->start();

	scheduleUpdate();
    return true;
}

void CastleFight::initDatabase()
{
	// Game settings??
	CastleData infoCastle{ { 0, EntityType::Castle, "Castle", { 3, 3 }, "castle.png", 1000.f, 0.f, {}, -1 } };
	
	VEntityActions builderActions;
	builderActions.push_back(std::make_unique<EntityAction>(EntityActionType::MoveTo, "", 0, "Move To"));
	builderActions.push_back(std::make_unique<ConstructBuildingAction>("", 0, "Build building", 2));
	builderActions.push_back(std::make_unique<EntityAction>(EntityActionType::HealRepairEntity, "", 0, "Repair"));
	builderActions.push_back(std::make_unique<EntityAction>(EntityActionType::AbortCurrent, "", 0, "Cancel"));

	BuilderData builderInfo{ { 1, EntityType::Builder,"Builder",{ 1, 1 }, "builder.png", 50.f, 0.f, builderActions, 0}, 100.f,{ 2 }, 0 };

	VEntityActions warriorActions;
	UnitData unitInfo{ { 3, EntityType::Warrior,"Basic warrior",{ 1, 1 }, "warrior.png", 50.f, 0.f, warriorActions, -1}, 100.f, 5., 10.f, 4.f, 0.5f };
	
	VEntityActions buildingActions;
	buildingActions.push_back(std::make_unique<SpawnUnitAction>("", 0, "Build warrior", 3));
	builderActions.push_back(std::make_unique<EntityAction>(EntityActionType::AbortCurrent, "", 0, "Cancel"));
	BuildingData buildingInfo{ {2, EntityType::Building,"Basic warrior barracks",{ 2, 2 }, "building0.png", 300.f, 5.f, buildingActions, 0} };

	_database.init(infoCastle, builderInfo, { buildingInfo }, { unitInfo });
}

void CastleFight::update(float dt)
{
	_playerController->update(dt, _input.get());
	_world->update(dt);
	_hud->update(dt);	
}



void CastleFight::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
	#endif
}
//
//void CastleFight::onMouseUp(cocos2d::EventMouse* mouseEvt) 
//{
//
//	Vec2 screenLocation = mouseEvt->getLocation();
//	Vec2 worldLocation = screenLocation;
//	auto visibleSize = Director::getInstance()->getVisibleSize();
//	worldLocation.y = visibleSize.height - worldLocation.y;
//
//	Castle* castleA = _teamA->getCastle();
//	Builder* builder = _teamA->getBuilder();
//
//
//	CCLOG("Touched @ SCREEN(%.02f, %.02f), WORLD(%.02f, %.02f)", screenLocation.x, screenLocation.y, worldLocation.x, worldLocation.y);
//	if (selectionType != SelectionType::Castle && castleA->view->getBoundingBox().containsPoint(worldLocation))
//	{
//		selectionType = SelectionType::Castle;
//	}
//	else if (selectionType != SelectionType::Builder && builder->intersects(worldLocation))
//	{
//		selectionType = SelectionType::Builder;
//	}
//	else
//	{
//		bool reset = true;
//		if (selectionType == SelectionType::Builder)
//		{
//			// Translate location to a coordinate:
//			int row = (int)screenLocation.y / 32;
//			int col = (int)screenLocation.x / 32;
//			bool walkable = _map->isWalkableTile({ col, row });
//			bool occupied = isOcuppiedTile({ col, row });
//			if ( walkable && !occupied)
//			{
//				CCLOG("Moving to @ %d, %d", col, row);
//				if (builder->getContext() == BuilderActionContext::Move)
//				{
//					builder->setTarget(col, row);
//					deleteConstructionCursor();
//				}
//				else if (builder->getContext() == BuilderActionContext::Build)
//				{
//					const BuildingData* building = _database.getBuildingData(builder->buildingID);
//					IntVec2 coords = { col, row };
//					builder->setClosestTarget({ col, row, building->size.x, building->size.y });
//					builder->setOnReachedTarget([this, building, coords, visibleSize](Builder* b) 
//					{
//						// it shouldn't have changed...
//						if (b->getContext() == BuilderActionContext::Build)
//						{
//							b->setContext(BuilderActionContext::Move);
//
//							Team* t = getTeamForBuilder(b);
//							if (t != nullptr)
//							{
//								t->addBuilding(building, coords, visibleSize, this);
//							}
//						}
//						deleteConstructionCursor();
//						b->setOnReachedTarget(nullptr);
//					});
//				}
//				reset = false;
//			}
//		}
//		if (reset)
//		{
//			selectionType = SelectionType::None;
//		}
//	}
//
//	_teamA->getCastle()->setSelectionState(selectionType == SelectionType::Castle);
//	builder->setSelected(selectionType == SelectionType::Builder);
//	if (selectionType != SelectionType::Builder)
//	{
//		_hud->hideConstructionCursor();
//	}
//}
//void CastleFight::onMouseDown(cocos2d::EventMouse*) {}
//void CastleFight::onMouseMove(cocos2d::EventMouse* mouseEvt)
//{
//	cocos2d::Vec2 location = mouseEvt->getLocation();
//	cocos2d::Size winSize = Director::getInstance()->getWinSizeInPixels();
//	if (location.x < 0)
//	{
//		location.x = 0;
//	}
//	else if (location.x >= winSize.width)
//	{
//		location.x = winSize.width - 1;
//	}
//	if (location.y < 0)
//	{
//		location.y = 0;
//	}
//	else if (location.y >= winSize.height)
//	{
//		location.y = winSize.height - 1;
//	}
//	_mousePos = location;
//}
//
//void CastleFight::onKeyReleased(cocos2d::EventKeyboard::KeyCode kc, cocos2d::Event* evt)
//{
//	if (selectionType == SelectionType::Builder)
//	{
//		Builder* builder = _teamA->getBuilder();
//		BuilderActionContext oldContext = builder->getContext();
//		if (kc == EventKeyboard::KeyCode::KEY_B)
//		{
//			if (oldContext != BuilderActionContext::Build)
//			{
//				builder->setContext(BuilderActionContext::Build);
//				// _hud->showConstructionCursor(builder->buildingID);
//				/*_constructionData = building;
//				_constructionCursor = DrawNode::create();
//				DrawNode* cursor = (DrawNode*)_constructionCursor;
//				cursor->drawSolidRect({ 0.0f, 0.0f }, { (float)(building->tileSize.x * building->size.x), (float)(-building->tileSize.y * building->size.y) }, Color4F(Color3B::GREEN, 0.5f));
//				addChild(_constructionCursor, 100);
//				cocos2d::Vec2 worldPos(_mousePos.x, Director::getInstance()->getWinSizeInPixels().height - _mousePos.y);
//				_constructionCursor->setPosition(worldPos);*/
//			}
//		}
//		else if (kc == EventKeyboard::KeyCode::KEY_ESCAPE)
//		{
//			builder->setContext(BuilderActionContext::Move);
//			// _hud->hideConstructionCursor();
//		}
//	}
//}

