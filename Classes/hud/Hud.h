#pragma once

#include "../world/entities/EntityTypes.h"
#include "../inputContexts/InputContextDefs.h"
#include "../world/entityActions/EntityActionExecutionListener.h"
#include "cocos/cocos2d.h"
#include <memory>
#include <functional>

class World;
class GameInput;

namespace cocos2d
{
	class Node;
	class Ref;
};

class Hud;
using HudPtr = std::unique_ptr<Hud>;

class Hud: public IEntityActionExecutionListener
{
	public:
		using CloseCallback = std::function<void(cocos2d::Ref* pSender)>;
		
		static HudPtr create(cocos2d::Node* parent);
		Hud(cocos2d::Node* parent);
		~Hud() = default;
		bool init(World* world, GameInput* input, const CloseCallback& closeCallback);
		
		bool addCloseButton(const CloseCallback& closeCallback);
		void update(float dt);

		void showConstructionCursor(EntityID id/* Target , database? */);
		void updateConstructionCursor(float dt);
		void hideConstructionCursor();
		void onContextAction(ContextActionType contextActionType, const InputContext* inputAction);

		virtual void onExecutionBegan(Entity* e) override;
		virtual void onExecutionFinished(Entity* e) override;
		virtual void onExecutionInterrupted(Entity* e) override;

		virtual void onCustom(Entity* e, CustomEntityActionExecutionData* data) override;

private:
	cocos2d::Node* _root = nullptr; // Change to CastleFightScene??

									// Other stuff. Consider having a "view" node.
	World* _world = nullptr;
	GameInput* _input = nullptr;
	cocos2d::Node* _constructionCursor = nullptr;
	EntityID _constructionCursorID = -1;
	CloseCallback _closeCallback;
	// Input?? 
};