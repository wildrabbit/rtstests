#include "Hud.h"
#include "../world/Map.h"
#include "../world/World.h"
#include "../world/WorldUtils.h"
#include "../cocos/cocos2d.h"
#include "../cocos/2d/CCMenuItem.h"
#include "../cocos/2d/CCMenu.h"
#include "../input/GameInput.h"
#include "../inputContexts/InputContexts.h"
#include "../world/entities/Builder.h"
#include "MathTypes.h"

USING_NS_CC;

Hud::Hud(cocos2d::Node* root)
	:_root(root)
{
}

HudPtr Hud::create(cocos2d::Node* parent)
{
	return std::make_unique<Hud>(parent);
}

bool Hud::init(World* world, GameInput* input, const CloseCallback& closeCallback)
{
	_world = world;
	_input = input;
	return addCloseButton(closeCallback);
}

bool Hud::addCloseButton(const CloseCallback& closeCallback)
{
	_closeCallback = closeCallback;
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto closeItem = MenuItemImage::create(
		"CloseNormal.png",
		"CloseSelected.png",
		_closeCallback);

	Size closeSize = closeItem->getContentSize();
	if (closeItem == nullptr ||
		closeSize.width <= 0 ||
		closeSize.height <= 0)
	{
		printf("Error");
		return false;
	}
	else
	{
		float x = origin.x + visibleSize.width - closeSize.width / 2;
		float y = origin.y + closeSize.height / 2;
		closeItem->setPosition(Vec2(x, y));
	}

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	_root->addChild(menu, 10);
	return true;
}

void Hud::update(float dt)
{
	// Update construction cursor if visible
	if (_constructionCursor)
	{
		updateConstructionCursor(dt);
	}
}

void Hud::updateConstructionCursor(float dt)
{
	cocos2d::Rect area = _constructionCursor->getBoundingBox();

	IntRect cursorArea = _world->getBuildingAreaAtPos(_input->mousePos, _constructionCursorID);
	const bool buildable = _world->isBuildableArea(cursorArea);
	Color4F cursorColour = buildable ? Color4F(Color3B::GREEN, 0.5f) : Color4F(Color3B::RED, 0.5f);
	IntVec2 tileSize = _world->map()->getTileSize();
	((DrawNode*)_constructionCursor)->drawSolidRect({ 0.0f, 0.0f }, { (float)( cursorArea.w * tileSize.y), (float)(-cursorArea.h * tileSize.y) }, cursorColour);
	cocos2d::Vec2 worldPos(cursorArea.x * tileSize.x, Director::getInstance()->getWinSize().height - cursorArea.y * tileSize.y);
	// REPLACE WITH cocos2d::Vec2 worldPos = mapUtils->convertCoordsToWorldPos(cursorArea, _world);
	_constructionCursor->setPosition(worldPos);
}

void Hud::showConstructionCursor(EntityID constructionID)
{
	if (_constructionCursor)
	{
		hideConstructionCursor();
	}
	
	_constructionCursorID = constructionID;
	_constructionCursor = DrawNode::create();
	DrawNode* cursor = (DrawNode*)_constructionCursor;
	updateConstructionCursor(0.f);
	IntRect cursorArea = _world->getBuildingAreaAtPos(_input->mousePos, _constructionCursorID);
	IntVec2 tileSize = _world->map()->getTileSize();

	cursor->drawSolidRect({ 0.0f, 0.0f }, { (float)(tileSize.x * cursorArea.w), (float)(-tileSize.y * cursorArea.w) }, Color4F(Color3B::GREEN, 0.5f));
	_root->addChild(_constructionCursor, 100);
}

void Hud::hideConstructionCursor()
{
	if (_constructionCursor != nullptr)
	{
		_constructionCursor->removeFromParent();
		_constructionCursor = nullptr;
		_constructionCursorID = -1;
	}
}

void Hud::onContextAction(ContextActionType contextActionType, const InputContext* inputContext)
{
	if (contextActionType == ContextActionType::Update)
	{
		const EntitySelectionContext* entityContext = dynamic_cast<const EntitySelectionContext*>(inputContext);
		if (!entityContext || entityContext->actionID != EntityActionType::ConstructBuilding)
		{
			hideConstructionCursor();
		}
		else
		{
			Builder* builder = dynamic_cast<Builder*>(entityContext->entity);
			if (builder != nullptr && _constructionCursor == nullptr)
			{
				showConstructionCursor(builder->getTargetBuildingID());
			}
		}
	}
	else
	{	
		// context change
		hideConstructionCursor();
	}
}

void Hud::onExecutionBegan(Entity* e)
{
	hideConstructionCursor();
}

void Hud::onExecutionFinished(Entity* e)
{
}

void Hud::onExecutionInterrupted(Entity* e)
{
}

void Hud::onCustom(Entity* e, CustomEntityActionExecutionData* data)
{
}